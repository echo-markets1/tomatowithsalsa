// Reference: https://cli.vuejs.org/config/#vue-config-js
const fs = require("fs");

module.exports = {
  runtimeCompiler: true, // Neccessary for vue2-circle-progress.
  css: {
    loaderOptions: {
      sass: {
        /*
         * Allows use of scss variables in single file components - aka for use
         * in the script tag of .vue files.
         * See this stackoverflow for reference:
         *   - https://stackoverflow.com/a/55583615
         * and the sass-loader documentation on github:
         *   - https://github.com/webpack-contrib/sass-loader/tree/v10.1.0#additionaldata
         */
        additionalData: `@import "@/assets/sass/01-abstracts/variables.scss";`,
      },
    },
  },
  // See https://app.clickup.com/2268607/v/dc/257dz-3385/257dz-2105?block=block-938ecf3b-4916-4d36-8f64-7335cc9ebc59
  // for more details on running locally over https
  devServer: {
    https: {
      key: fs.readFileSync("./certs/localhost+1-key.pem"),
      cert: fs.readFileSync("./certs/localhost+1.pem"),
    },
    public: "https://localhost:8080/",
  },
};
