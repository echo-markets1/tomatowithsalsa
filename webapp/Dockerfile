## Build Stage ##

# Builds the production ready artifact

# Run against the alpine linux node image, preferred for minimal image size
# See https://hub.docker.com/_/node?tab=description&amp%3Bname=alpine&page=1&ordering=last_updated
# and https://alpinelinux.org/ for more details
FROM node:lts-alpine as build-stage

# Make the 'webapp' folder the current working directory
WORKDIR /webapp

# Copy both 'package.json' and 'package-lock.json' (if available)
COPY package*.json ./

# Install project dependencies
# Fixes issues caused by npm-gyp. See: 
# - https://github.com/nodejs/docker-node/issues/282
# - https://github.com/nodejs/docker-node/blob/main/docs/BestPractices.md#node-gyp-alpine
# - https://stackoverflow.com/questions/54428608/docker-node-alpine-image-build-fails-on-node-gyp
# for more details
# --no-cache: download package index on-the-fly, no need to cleanup afterwards
# --virtual: bundle packages, remove whole bundle at once, when done
RUN apk add --no-cache --virtual .gyp python3 make g++ \
    && npm install \
    && apk del .gyp

# Copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .

# Build app for production with minification
RUN npm run build

## Production State ##

# Serve the build artifact using NGINX
FROM nginx:stable-alpine as production-stage

# Copy the build artifacts to be served by NGINX
COPY --from=build-stage /webapp/dist /usr/share/nginx/html

# Expose port 80 to serve the content
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]