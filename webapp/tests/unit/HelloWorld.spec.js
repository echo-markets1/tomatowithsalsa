import { shallowMount } from "@vue/test-utils";
import HelloWorld from "@/components/HelloWorld.vue";

describe("HelloWorld Component Test", () => {
  it("renders message when component is created", () => {
    // render the component
    const wrapper = shallowMount(HelloWorld, {
      propsData: {
        msg: "Vue Test"
      }
    });

    // check that the title is rendered
    expect(wrapper.text()).toContain("Vue Test");
  });
});
