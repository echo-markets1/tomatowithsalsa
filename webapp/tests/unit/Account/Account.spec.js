// Always
import { shallowMount, mount, createLocalVue } from "@vue/test-utils";
// This line is to allow us to use async await syntax: without tests will throw errors.
// Babel should take care of this at run time, no need to import outside of testing
import "babel-polyfill";
// Import component being tested
import Account from "@/views/Account.vue";
import StatsDetailCard from "@/components/account/StatsDetailCard.vue";

// We need to mock both the axios library for our async calls and the Vuex store
import mockAxios from "jest-mock-axios";
// import flushPromises from "flush-promises";
//Create a local vue instance with which to mock Vuex store with
import Vuex from "vuex";
const localVue = createLocalVue();
localVue.use(Vuex);
// We also need to set up the router instance to test some functionality
import VueRouter from "vue-router";
localVue.use(VueRouter);
const router = new VueRouter();

// Set up mock data to use
const mockAccount = {
  id: "3db1b061-32e6-4d6c-8fe1-ddda4493b233",
  email: "jp@echo-markets.com",
  first_name: "James",
  last_name: "Pala",
  value_liquid: 30000,
  value_invested: 0,
  total_value: 30000,
  value_invested_change: 0,
  value_at_last_logout: null,
  account_type: "Alpha User",
  date_of_birth: null,
  is_staff: true,
  has_completed_user_agreement: false,
  has_completed_onboarding: false,
  can_publish: false,
  has_bucket: false,
  settings: {
    alerts_on: true,
    alert_frequency: "D",
    alert_frequency_options: [
      { value: "D", text: "Daily" },
      { value: "W", text: "Weekly" },
      { value: "M", text: "Monthly" },
    ],
  },
};
const mockAbout = {
  id: "3db1b061-32e6-4d6c-8fe1-ddda4493b233",
  full_name: "James Pala",
  short_name: "James P",
  description: "",
  awards: [],
  followers_count: 0,
  views: 0,
  best_day: 0,
  worst_day: 0,
  total_trades: 0,
  can_pattern_day_trade: true,
};
const mockProfile = {
  date_joined: "2020-10-21T18:01:52.717323Z",
  date_of_birth: null,
  gender: "",
  gender_options: [
    { value: "", text: "Select a gender" },
    { value: "M", text: "Male" },
    { value: "F", text: "Female" },
    { value: "O", text: "Other" },
  ],
  reason_for_investing: "",
  marital_status: "S",
  marital_status_options: [
    { value: "", text: "Select your marital status" },
    { value: "S", text: "Single" },
    { value: "M", text: "Married" },
    { value: "L", text: "Living together" },
    { value: "N", text: "No longer married" },
    { value: "W", text: "Widowed" },
  ],
  disney_princess: "AR",
  disney_princess_options: [
    { value: "", text: "Select a disney princess" },
    { value: "S", text: "Snow White (Snow White and the Seven Dwarfs)" },
    { value: "C", text: "Cinderella (Cinderella)" },
    { value: "AU", text: "Aurora (Sleeping Beauty)" },
    { value: "AR", text: "Ariel (The Little Mermaid)" },
    { value: "B", text: "Belle (Beauty and the Beast)" },
    { value: "J", text: "Jasmine (Aladdin)" },
    { value: "P", text: "Pocahontas (Pocahontas)" },
    { value: "M", text: "Mulan (Mulan)" },
    { value: "T", text: "Tiana (The Princess and the Frog)" },
    { value: "R", text: "Rapunzel (Tangled)" },
    { value: "ME", text: "Merida (Brave)" },
    { value: "MO", text: "Moana (Moana)" },
  ],
  education_level: "",
  education_level_options: [
    { value: "", text: "Select an education level" },
    { value: "HS", text: "Some high school or less" },
    { value: "HG", text: "High school graduate or equivalent (GED)" },
    { value: "CN", text: "Some college, no degree" },
    { value: "CI", text: "Some college, in progress" },
    { value: "B", text: "College graduate, (Bachelors degree)" },
    { value: "GN", text: "Graduate school, no degree" },
    { value: "GI", text: "Graduate school, in progress" },
    { value: "M", text: "Masters degree" },
    { value: "P", text: "Professional degree" },
    { value: "D", text: "Doctorate" },
  ],
  household_income_level: "3",
  household_income_level_options: [
    { value: "", text: "Select a household income level" },
    { value: "0", text: "Less than $25,000 per year" },
    { value: "1", text: "$25,000 - $49,999 per year" },
    { value: "2", text: "$50,000 - $74,999 per year" },
    { value: "3", text: "$75,000 - $99,999 per year" },
    { value: "4", text: "$100,000 - $124,999 per year" },
    { value: "5", text: "$125,000 - $149,999 per year" },
    { value: "6", text: "$150,000 - $174,999 per year" },
    { value: "7", text: "$175,000 - $199,999 per year" },
    { value: "8", text: "$200,000 or more per year" },
  ],
  residential_status: "",
  residential_status_options: [
    { value: "", text: "Select a residential status" },
    { value: "PR", text: "Living with parents or relatives" },
    { value: "CS", text: "Couch surfing" },
    { value: "SR", text: "Renting, splitting with others" },
    { value: "RE", text: "Renting my own place" },
    { value: "CH", text: "Campus housing" },
    { value: "CO", text: "I own an condo" },
    { value: "HO", text: "I own a house" },
  ],
  preferred_olympics: "W",
  preferred_olympics_options: [
    { value: "", text: "Select your preferred olympic games" },
    { value: "S", text: "Summer olympic games" },
    { value: "W", text: "Winter olympic games" },
    {
      value: "R",
      text:
        "The olympics are soft, we should bring back the Roman Gladiator Games",
    },
  ],
  number_of_household_adults: 0,
  number_of_household_children: 0,
  occupation: "",
};

// Mock the store
const store = new Vuex.Store({
  modules: {
    auth: {
      status: {
        loggedIn: true,
      },
      namespaced: true,
    },
    accounts: {
      state: {
        currentAccount: mockAccount,
      },
      actions: {
        getCurrentAccount: jest.fn(),
        getCurrentUser: jest.fn(),
      },
      namespaced: true,
    },
    buckets: {
      state: {
        publicBuckets: [],
        userBuckets: [],
      },
      actions: {
        set__publicBuckets: jest.fn(),
        set__userBuckets: jest.fn(),
      },
      namespaced: true,
    },
  },
});

let responseObj = {
  data: {
    about: mockAbout,
    profile: mockProfile,
    account: mockAccount,
  },
};

describe("Account.vue", () => {
  const wrapper = mount(Account, { store, router, localVue });

  mockAxios.mockResponse(responseObj);

  // Shallow mounting does not set up child components, but rather stubs them
  const shallowWrapper = shallowMount(Account, { store, router, localVue });

  it("Page component exists", async () => {
    // debugger

    expect(wrapper.findComponent(Account).exists()).toBe(true);
  });

  it("Page component exists (again with shallow mounted)", async () => {
    expect(shallowWrapper.findComponent(Account).exists()).toBe(true);
  });

  it("renders StatsDetailCard component", async () => {
    expect(wrapper.findComponent(StatsDetailCard).exists()).toBe(true);
  });

  it(" has the tab nav", () => {
    expect(shallowWrapper.find("#account-tabs-nav").exists()).toBe(true);
  });

  it(" has the about me tab nav", () => {
    expect(shallowWrapper.find("#about-me-tab").exists()).toBe(true);
  });

  it(" has the account tab nav", () => {
    expect(shallowWrapper.find("#account-tab").exists()).toBe(true);
  });

  it(" has the profile tab nav", () => {
    expect(shallowWrapper.find("#profile-tab").exists()).toBe(true);
  });

  it(" renders ChangeNameFormModal", () => {});

  it(" shows ChangeNameFormModal when first or last name rows are clicked", () => {});

  //same with password and email

  it(" shows success alert when save-account-successful is emitted", () => {});

  it(" shows failure alert when save-account-failure is emitted", () => {});
});
