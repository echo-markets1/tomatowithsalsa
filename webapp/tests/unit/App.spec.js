import { shallowMount, createLocalVue } from '@vue/test-utils'
import App from './../../src/App.vue'
import Vuex from 'vuex'
//
import VueRouter from "vue-router";

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter);

const router = new VueRouter()

describe('App.vue', () => {
  const store = new Vuex.Store({
    modules: {
      auth: {
        status: {
          loggedIn: true
        },
        namespaced: true,
      },
      accounts: {
        state: {
          currentAccount: null
        },
        actions: {
          getCurrentAccount: jest.fn()
        },
        namespaced: true
      }
    }
  })

  // beforeEach(() => {
  //   actions = {
  //     accounts:{
  //       getCurrentAccount: jest.fn(),
  //     }
  //   }
  // })

  const wrapper = shallowMount(
      App,
      { store, router, localVue },
    );

  it('component exists', () => {
    expect(wrapper.findComponent(App).exists()).toBe(true)
    expect(wrapper.findComponent(App).vm).toBeTruthy()
  })

})