import clickOutside from "@/directives/click-ouside.js";
import addInputValidationClass from "@/directives/add-input-validation-class.js";

/**
 * You can register global directives here and use them as a plugin in your main Vue instance
 */

const GlobalDirectives = {
  install(Vue) {
    Vue.directive("click-outside", clickOutside);
    Vue.directive("add-input-validation-class", addInputValidationClass);
  },
};

export default GlobalDirectives;
