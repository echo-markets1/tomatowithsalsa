import BaseButton from "@/components/base/BaseButton.vue";
import BaseDropdown from "@/components/base/BaseDropdown.vue";
import Badge from "@/components/base/Badge.vue";
import BaseHeader from "@/components/base/BaseHeader.vue";
import BaseHeaderTabs from "@/components/base/BaseHeaderTabs.vue";
import BaseOverviewHeader from "@/components/base/BaseOverviewHeader.vue";
import BaseSwitch from "@/components/base/BaseSwitch.vue";
import BaseInput from "@/components/base/BaseInput.vue";
import SingleSelect from "@/components/base/SingleSelect.vue";
import BaseModalForm from "@/components/base/BaseModalForm.vue";
import InfoTooltip from "@/components/base/tooltips/InfoTooltip.vue";
import BaseAvatar from "@/components/base/BaseAvatar.vue";
import DecorativeCircle from "@/components/base/graphics/DecorativeCircle.vue";
import BaseCardCarousel from "@/components/base/BaseCardCarousel.vue";
import IconButton from "@/components/base/IconButton.vue";
import Editor from "@/components/editor/Editor";

/**
 * Register global components here to use them as a plugin in your main Vue instance
 * This is also where most components imported from Creative Tim will be registered
 */

const GlobalComponents = {
  install(Vue) {
    Vue.component(BaseButton.name, BaseButton);
    Vue.component(BaseDropdown.name, BaseDropdown);
    Vue.component(Badge.name, Badge);
    Vue.component(BaseHeader.name, BaseHeader);
    Vue.component(BaseHeaderTabs.name, BaseHeaderTabs);
    Vue.component(BaseOverviewHeader.name, BaseOverviewHeader);
    Vue.component(BaseSwitch.name, BaseSwitch);
    Vue.component(BaseInput.name, BaseInput);
    Vue.component(SingleSelect.name, SingleSelect);
    Vue.component(BaseModalForm.name, BaseModalForm);
    Vue.component(InfoTooltip.name, InfoTooltip);
    Vue.component(BaseAvatar.name, BaseAvatar);
    Vue.component(DecorativeCircle.name, DecorativeCircle);
    Vue.component(BaseCardCarousel.name, BaseCardCarousel);
    Vue.component(IconButton.name, IconButton);
    Vue.component(Editor.name, Editor);
  },
};

export default GlobalComponents;
