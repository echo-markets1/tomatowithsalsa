import AuthService from "@/services/auth.service";

const JWT = JSON.parse(localStorage.getItem("JWT"));
const initialState = JWT
  ? { status: { loggedIn: true }, JWT }
  : { status: { loggedIn: false }, JWT: null };

export const auth = {
  namespaced: true,
  state: initialState,
  actions: {
    login({ commit, dispatch }, user) {
      return AuthService.login(user).then(
        (JWT) => {
          commit("loginSuccess", JWT);
          // Upon successful login, trigger the initialization of the store
          dispatch("initialize", null, { root: true });
          return Promise.resolve(JWT);
        },
        (error) => {
          commit("loginFailure");
          return Promise.reject(error);
        }
      );
    },
    refreshToken({ commit }) {
      return AuthService.refreshToken().then(
        (JWT) => {
          commit("refreshSuccess", JWT);
          return Promise.resolve(JWT.access);
        },
        (error) => {
          commit("loginFailure");
          return Promise.reject(error);
        }
      );
    },
    logout({ commit }) {
      AuthService.logout();
      commit("logout");
    },
    signup({ commit }, user) {
      return AuthService.signup(user).then(
        (response) => {
          commit("signupSuccess");
          return Promise.resolve(response.data);
        },
        (error) => {
          commit("signupFailure");
          return Promise.reject(error);
        }
      );
    },
  },
  mutations: {
    loginSuccess(state, JWT) {
      state.status.loggedIn = true;
      state.JWT = JWT;
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      state.JWT = null;
    },
    logout(state) {
      state.status.loggedIn = false;
      state.JWT = null;
    },
    refreshSuccess(state, JWT) {
      state.status.loggedIn = true;
      state.JWT = JWT;
    },
    refreshFailure(state) {
      state.status.loggedIn = false;
      state.JWT = null;
    },
    signupSuccess(state) {
      state.status.loggedIn = false;
    },
    signupFailure(state) {
      state.status.loggedIn = false;
    },
  },
};
