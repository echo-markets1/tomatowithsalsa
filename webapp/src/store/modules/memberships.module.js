import { MEMBERSHIPS as types } from "@/store/mutation-types.js";
import GroupsDataService from "@/services/groups-data.service";
import Vue from "vue";

export const memberships = {
  namespaced: true,
  state: {},
  mutations: {
    [types.UPDATE_MEMBERSHIPS](state, { memberships, userId }) {
      if (!Object.prototype.hasOwnProperty.call(state, userId)) {
        Vue.set(state, userId, []);
      }
      state[userId] = memberships.reduce(function(userMemberships, membership) {
        delete membership["user_id"];
        userMemberships.push(membership);
        return userMemberships;
      }, []);
    },
  },
  actions: {
    async get({ state, commit }, userId) {
      let memberships = state[userId] || null;
      if (memberships) {
        return memberships;
      } else {
        try {
          const response = await GroupsDataService.getMemberships(userId);
          memberships = response.data;
          commit(types.UPDATE_MEMBERSHIPS, { memberships, userId });
          return memberships;
        } catch (e) {
          console.log(
            `Could not find memberships for user with id: ${userId}. Returning an empty membership. ${e}`
          );
          // Return an "empty" array if no memberships are found
          return [];
        }
      }
    },
  },
};
