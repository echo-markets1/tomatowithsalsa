import { STOCK_QUOTES as types, ADD_SYMBOL } from "@/store/mutation-types.js";
import { shouldRefreshTimeseries } from "@/store/timeseries-refresh-checker.js";
import { GRANULARITIES } from "@/constants/twelvedata-constants.js";
import QuoteService from "@/services/market-data/quote.service";
import Vue from "vue";

export const stockQuotes = {
  namespaced: true,
  state: {
    /*
        ticker_symbol: daily quote
        Ex:
        'AAPL': {
            "name": "Apple Inc",
            "exchange": "NASDAQ",
            "currency": "USD",
            "datetime": "2019-08-09",
            "open": "201.30000",
            "high": "202.75999",
            "low": "199.28999",
            "close": "200.99001",
            "volume": "24619700",
            "previous_close": "203.42999",
            "change": "-2.43998",
            "percent_change": "-1.19942",
            "average_volume": "203734",
            "fifty_two_week": {
                "low": "142.19",
                "high": "232.07",
                "low_change": "58.8",
                "high_change": "-31,08",
                "low_change_percent": "41.35312",
                "high_change_percent": "-13.39251",
                "range": "142.19 - 232.07"
            }
        }
      */
  },
  getters: {},
  mutations: {
    /**
     * Commits daily quote info to the store, keyed by symbol
     * @param {Object} state
     * @param {Object} quote - Object containing daily quote for a given symbol
     */
    [types.ADD_STOCK_DAILY_QUOTE](state, quote) {
      const symbol = quote.symbol;
      delete quote.symbol;
      Vue.set(state, symbol, quote);
    },
  },
  actions: {
    /**
     * Fetches daily quote info for a given symbol
     * @param {Object} context
     * @param {String} symbol - The unique identifier of the stock
     */
    async fetch_daily_quote__for_symbol({ state, commit }, symbol) {
      let lastTimestamp = 0;
      try {
        lastTimestamp = new Date(state[symbol].datetime).getTime();
      } catch {
        // There is no last timestamp - pass
      }
      if (
        shouldRefreshTimeseries(
          GRANULARITIES.ONE_DAY,
          lastTimestamp,
          new Date()
        )
      ) {
        const response = await QuoteService.getQuote(symbol);
        if (response.data.status !== "error") {
          commit(types.ADD_STOCK_DAILY_QUOTE, response.data);
        } else {
          console.log(
            `WARN: twelvedata threw an error while retrieving quote data for symbol: ${symbol}`
          );
          commit(`unsupportedStocks/${ADD_SYMBOL}`, symbol, { root: true });
        }
      }
    },
  },
};
