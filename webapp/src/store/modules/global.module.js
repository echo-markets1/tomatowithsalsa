/* This module respresents the highest level, or root state of our application.
 */
export const global = {
  // Rather than namespacing as global, let this be the root state
  state: {
    isSidebarVisible: false,
  },
  getters: {
    isSidebarVisible: (state) => {
      return state.isSidebarVisible;
    },
  },
  mutations: {
    set__isSidebarVisible: (state, isVisible) => {
      state.isSidebarVisible = isVisible;
    },
  },
  actions: {
    async show__sidebar({ commit }) {
      commit("set__isSidebarVisible", true);
    },
    async hide__sidebar({ commit }) {
      commit("set__isSidebarVisible", false);
    },
  },
};
