import RiskMetricsService from "@/services/risk-metrics.service.js";
import { RISK_METRICS as types } from "@/store/mutation-types.js";
import { riskMetrics } from "@/store/modules/risk-metrics/risk-metrics.module.js";
export const portfolioRiskMetrics = {
  namespaced: true,
  modules: {
    riskMetrics,
  },
  actions: {
    // To be called once the userPortfolios have been initialized
    async initialize({ commit, rootGetters }) {
      const promises = [];
      // Asynchronously retrieve the risk metrics for each portfolio by it's id
      rootGetters["portfolios/userPortfolios/all_portfolioIds"].forEach(
        (id) => {
          promises.push(RiskMetricsService.getPortfolioRiskMetrics(id));
        }
      );
      // Once all the calls have resolved, commit the composite response
      const responses = await Promise.all(promises);
      commit(
        types.LOAD_RISK_METRICS,
        responses.map((response) => response.data)
      );
    },
    // To be called once a new portfolio has been linked
    async load_single_portfolio_metrics__by_id({ commit }, portfolioId) {
      const response = await RiskMetricsService.getPortfolioRiskMetrics(
        portfolioId
      );
      commit(types.LOAD_RISK_METRICS, [response]);
    },
  },
};
