import { createLookupFromListByKey } from "@/store/create-lookup-from-list-by-key.js";
import { RISK_METRICS as types } from "@/store/mutation-types.js";

// A common riskMetrics module to abstract shared logic across riskMetric modules
export const riskMetrics = {
  namespaced: false,
  // State must be a function in order to be reusable across multiple modules
  state: () => ({}),
  mutations: {
    [types.LOAD_RISK_METRICS](state, payload) {
      /*
       * Accepts a payload of a list of risk calucalations, keyed by an entity's
       * identifier and loops through each identifier, flattening the list of
       * riskmetrics for each entity into object keyed by id, then period.
       * Example payload:
          [ 
            "03vY12a9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw": [
              {
                alpha: 0.005757961307796289,
                beta: 1.5156370450783634,
                standard_deviation: 21.422090196148414,
                sharpe: -1.0676800044187345,
                r_squared: 0.3348437934173201,
                period: "1Y",
              },
              {
                alpha: ...,
                period: "3Y",
              },
            ],
            "1123a49r5pH45dV9vmAbtxnn1jZ5bYHe5w7kw": [
              {
                alpha: 0.005757961307796289,
                beta: 1.5156370450783634,
                standard_deviation: 21.422090196148414,
                sharpe: -1.0676800044187345,
                r_squared: 0.3348437934173201,
                period: "1Y",
              },
              {
                alpha: ...,
                period: "3Y",
              },
            ],
          ]
       * 
       * Example flattened output:
         { 
           "03vY12a9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw": [
             {
               "1Y": {
                 alpha: 0.005757961307796289,
                 beta: 1.5156370450783634,
                 standard_deviation: 21.422090196148414,
                 sharpe: -1.0676800044187345,
                 r_squared: 0.3348437934173201
               }
             },
             {
               "3Y": {
                 alpha: ...,
               }
             },
           ],
         }
       */
      // Loop through each entity
      payload.forEach(function(riskMetrics) {
        // Extract the id (key) and list of metrics (value) from the entry of this object
        const [idMetricsTuple] = Object.entries(riskMetrics);
        const id = idMetricsTuple[0];
        const metrics = idMetricsTuple[1];
        // Flatten the list of metrics to be keyed by the period
        const metricsByPeriod = createLookupFromListByKey(
          {},
          metrics,
          "period"
        );
        state[id] = Object.assign({}, metricsByPeriod);
      });
    },
  },
};
