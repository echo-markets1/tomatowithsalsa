import { groupPostDataService } from "@/services/post-data.service";
import { POSTS as types } from "@/store/mutation-types.js";
import { updateObjectInList } from "@/store/update-object-in-list.js";

export const groupPosts = {
  namespaced: true,
  state: {
    groupData: {
      /*[groupId]: {
        totalPages,
        new: {
          nextPage: 1,
          posts: []
        },
        top: {
          nextPage: 1,
          posts: []
        }
      }*/
    },
    orderBy: "top",
    limit: 5,
  },
  getters: {
    post__fromGroupId_andPostId: (state) => ({ groupId, postId }) => {
      // Default ot empty list so find can return undefined if no matching groupId
      const posts = state.groupData[groupId]?.[state.orderBy].posts || [];
      // Returns undefined if no matching post
      return posts.find((post) => post.id == postId);
    },
    getPosts(state, orderBy) {
      return state.groupData[orderBy].posts;
    },
  },
  mutations: {
    [types.CREATE_NEW_POST](state, { newPost, groupId }) {
      const posts = state.groupData[groupId][state.orderBy].posts;

      if (state.orderBy === "new") {
        posts.pop();
        posts.unshift(newPost);
      }
      // Add the new (most recent) post to the top of the list
      state.groupData[groupId][state.orderBy].posts = posts;
    },
    /*
     * The RESPOND_TO_POST, TOGGLE_POST_LIKE, and TOGGLE_POST_PIN mutations are
     * and likely will remain identical. Prefer to use unique mutations for each
     * for enhanced tracking and visibility
     */
    [types.RESPOND_TO_POST](state, { superPost, groupId }) {
      // updateObjectInList(superpost, state.posts[groupId]);
      // state.groupData = Object.assign({}, state.groupData, {
      //   [groupId]: state.groupData[groupId],
      // });
      const postList = state.groupData[groupId][state.orderBy].posts;
      updateObjectInList(superPost, postList);
    },
    [types.TOGGLE_POST_LIKE](state, { updatedPost, groupId, isReply }) {
      if (state.groupData[groupId]) {
        let postList = state.groupData[groupId][state.orderBy].posts;
        if (isReply) {
          const superPostIndex = postList.findIndex(
            (post) => post.id == updatedPost.superpost
          );
          postList = postList[superPostIndex].replies;
        }
        updateObjectInList(updatedPost, postList);
      }
    },
    [types.TOGGLE_POST_PIN](state, { updatedPost, groupId }) {
      if (state.groupData[groupId]) {
        let postList = state.groupData[groupId][state.orderBy].posts;
        updateObjectInList(updatedPost, postList);
      }
    },
    [types.SET_TOTAL_PAGES](state, { groupId, totalPages }) {
      const data = state.groupData[groupId];

      if (!data) {
        state.groupData = Object.assign({}, state.groupData, {
          [groupId]: {
            totalPages,
            new: {
              nextPage: 1,
              posts: [],
            },
            top: {
              nextPage: 1,
              posts: [],
            },
          },
        });
      } else {
        state.groupData[groupId].totalPages = totalPages;
      }
    },
    [types.SET_NEXT_PAGE](state, { groupId, nextPage }) {
      state.groupData[groupId][state.orderBy].nextPage = nextPage;
    },
    [types.LOAD_POSTS](state, { groupId, posts }) {
      const groupPosts = state.groupData[groupId][state.orderBy].posts;
      state.groupData[groupId][state.orderBy].posts = [...groupPosts, ...posts];
    },
    [types.SET_ORDER_BY](state, orderBy) {
      state.orderBy = orderBy;
    },
  },
  actions: {
    setOrderBy({ commit }, orderBy) {
      commit(types.SET_ORDER_BY, orderBy);
    },
    setNextPage({ commit }, { groupId, nextPage }) {
      commit(types.SET_NEXT_PAGE, { groupId, nextPage });
    },
    async getTotalPages({ commit, state }, groupId) {
      const response = await groupPostDataService.getPosts({ groupId });
      const totalPages = Math.ceil(response.data.count / state.limit);

      commit(types.SET_TOTAL_PAGES, { groupId, totalPages });
    },
    async loadGroupPosts({ commit, state }, groupId) {
      let response = null;
      switch (state.orderBy) {
        case "top": {
          response = await groupPostDataService.getTopPosts({
            groupId,
            pageSize: state.limit,
            pageNumber: state.groupData[groupId][state.orderBy].nextPage,
          });
          break;
        }
        case "new": {
          response = await groupPostDataService.getNewestPosts({
            groupId,
            pageSize: state.limit,
            pageNumber: state.groupData[groupId][state.orderBy].nextPage,
          });
          break;
        }
      }

      const posts = response.data.results;
      commit(types.LOAD_POSTS, { groupId, posts });
    },
    async createPost({ commit }, { groupId, message }) {
      const response = await groupPostDataService.createPost({
        groupId,
        message,
      });
      const newPost = response.data;
      commit(types.CREATE_NEW_POST, { newPost, groupId });
    },
    async replyPost({ commit }, { groupId, superPostId, message }) {
      const response = await groupPostDataService.replyToPost({
        groupId,
        superPostId,
        message,
      });
      const superPost = response.data;
      commit(types.RESPOND_TO_POST, { superPost, groupId });
    },
    async likePost({ commit }, { postId, groupId, isReply }) {
      const response = await groupPostDataService.likePost({ postId });
      const updatedPost = response.data;

      commit(types.TOGGLE_POST_LIKE, { updatedPost, groupId, isReply });
      commit("publicPosts/TOGGLE_POST_LIKE", updatedPost, { root: true });
    },
    async pinPost({ commit }, { groupId, postId }) {
      const response = await groupPostDataService.pinPost({ postId });
      const updatedPost = response.data;

      commit(types.TOGGLE_POST_PIN, { updatedPost, groupId });
      commit("publicPosts/TOGGLE_POST_PIN", updatedPost, { root: true });
    },
  },
};
