import { postDataService } from "@/services/post-data.service";
import { POSTS as types } from "@/store/mutation-types.js";
import { updateObjectInList } from "@/store/update-object-in-list.js";

export const publicPosts = {
  namespaced: true,
  state: {
    publicData: {
      totalPages: 0,
      new: {
        nextPage: 1,
        posts: [],
      },
      top: {
        nextPage: 1,
        posts: [],
      },
    },
    orderBy: "top",
    limit: 5,
  },
  mutations: {
    [types.LOAD_POSTS](state, posts) {
      const publicPosts = state.publicData[state.orderBy].posts;
      state.publicData[state.orderBy].posts = [...publicPosts, ...posts];
    },
    [types.SET_NEXT_PAGE](state, nextPage) {
      state.publicData[state.orderBy].nextPage = nextPage;
    },
    [types.SET_ORDER_BY](state, orderBy) {
      state.orderBy = orderBy;
    },
    [types.SET_TOTAL_PAGES](state, totalPages) {
      state.publicData.totalPages = totalPages;
    },
    [types.TOGGLE_POST_LIKE](state, updatedPost) {
      if (state.publicData[state.orderBy]) {
        const postList = state.publicData[state.orderBy].posts;
        updateObjectInList(updatedPost, postList);
      }
    },
    [types.TOGGLE_POST_PIN](state, updatedPost) {
      if (state.publicData[state.orderBy]) {
        const postList = state.publicData[state.orderBy].posts;
        updateObjectInList(updatedPost, postList);
      }
    },
  },
  actions: {
    async loadPublicPosts({ commit, state }) {
      let response = null;
      switch (state.orderBy) {
        case "top": {
          response = await postDataService.getTopPosts({
            pageSize: state.limit,
            pageNumber: state.publicData[state.orderBy].nextPage,
          });
          break;
        }
        case "new": {
          response = await postDataService.getNewestPosts({
            pageSize: state.limit,
            pageNumber: state.publicData[state.orderBy].nextPage,
          });
          break;
        }
      }

      const posts = response.data.results;
      commit(types.LOAD_POSTS, posts);
    },
    setOrderBy({ commit }, orderBy) {
      commit(types.SET_ORDER_BY, orderBy);
    },
    setNextPage({ commit }, nextPage) {
      commit(types.SET_NEXT_PAGE, nextPage);
    },
    async getTotalPages({ commit, state }) {
      const response = await postDataService.getPosts({});
      const totalPages = Math.ceil(response.data.count / state.limit);

      commit(types.SET_TOTAL_PAGES, totalPages);
    },
  },
};
