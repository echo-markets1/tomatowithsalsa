import { userAccount } from "@/store/modules/accounts/user-account.module";
import AccountsService from "@/services/accounts.service";
import { createLookupFromListByKey } from "../create-lookup-from-list-by-key";
import { ACCOUNTS as types } from "../mutation-types";
export const accounts = {
  namespaced: true,
  modules: {
    userAccount,
  },
  state: {
    accountsByGroup: {
      /*
      groupId1: [
        userObj1,
        userObj2,
        ...
      ],
      groupId2: [
        userObj1,
        userObj2,
        ...
      ],
     */
    },
    allAccounts: {
      /*
      userId1: userObj1,
      userId2: userObj2,
      ...
      */
    },
  },
  getters: {
    userAccount: (state) => {
      return state.userAccount.account;
    },
    getGroupUsers: (state) => (groupId) => {
      return state.accountsByGroup[groupId];
    },
  },
  mutations: {
    [types.LOAD_ACCOUNTS](state, accounts) {
      state.allAccounts = createLookupFromListByKey(
        state.allAccounts,
        accounts,
        "id"
      );
    },
    setAccountsByGroup(state, { groupId, users }) {
      state.accountsByGroup = Object.assign({}, state.accountsByGroup, {
        [groupId]: createLookupFromListByKey(
          state.accountsByGroup[groupId],
          users,
          "id"
        ),
      });
    },
  },
  actions: {
    async initialize({ dispatch }) {
      return dispatch("userAccount/initialize");
    },
    async getAccountsByGroup({ commit }, groupId) {
      const response = await AccountsService.getUsers({ groupId });
      const users = response.data.results;

      commit("setAccountsByGroup", { groupId, users });
      return users;
    },
    async getAllAccounts({ commit }) {
      const response = await AccountsService.getUsers({});
      commit(types.LOAD_ACCOUNTS, response.data.results);
    },
    async get({ state, commit }, userId) {
      let account = state.accountsByGroup[userId] || state.allAccounts[userId];
      if (account) {
        return account;
      } else {
        try {
          const response = await AccountsService.getUser({ userId });
          account = response.data;
          commit(types.LOAD_ACCOUNTS, [account]);
          return account;
        } catch (e) {
          console.log(
            `Could not find account with userId: ${userId}. Returning an empty account. ${e}`
          );
          // Return an "empty" account if none are found
          return {};
        }
      }
    },
  },
};
