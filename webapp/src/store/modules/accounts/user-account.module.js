import AccountsService from "@/services/accounts.service";
import { ACCOUNTS as types } from "@/store/mutation-types.js";
export const userAccount = {
  namespaced: true,
  state: {
    account: {
      /*
       * Predefining these nested objects allows to write computed properties against
       * these state objects without throwing errors at create time. The alternative
       * here is to write getters to access these nested state objects.
       */
      profile: {},
      settings: {},
    },
  },
  mutations: {
    [types.LOAD_ACCOUNT](state, account) {
      state.account = Object.assign({}, state.account, account);
    },
    [types.UPDATE_ACCOUNT](state, payload) {
      state.account = Object.assign({}, state.account, payload);
    },
  },
  actions: {
    async initialize({ commit }) {
      const account = await AccountsService.getCurrentAccount();
      commit(types.LOAD_ACCOUNT, account.data);
    },
    async update__account_handle({ commit }, handle) {
      // We only want to commit the mutation if the update was successful
      await AccountsService.updateHandle(handle);
      commit(types.UPDATE_ACCOUNT, { handle: handle });
    },
    async update__account_firstname({ commit }, name) {
      // We only want to commit the mutation if the update was successful
      await AccountsService.updateName(name);
      commit(types.UPDATE_ACCOUNT, { first_name: name });
    },
    async update__account_lastname({ commit }, name) {
      // We only want to commit the mutation if the update was successful
      await AccountsService.updateName(null, name);
      commit(types.UPDATE_ACCOUNT, { last_name: name });
    },
    async update__account_email(
      { commit },
      { newEmail1, newEmail2, password }
    ) {
      // We only want to commit the mutation if the update was successful
      await AccountsService.updateEmail(newEmail1, newEmail2, password);
      commit(types.UPDATE_ACCOUNT, {
        email: newEmail1,
      });
    },
    async update__account_date_of_birth({ state, commit }, dateOfBirth) {
      const profile = Object.assign({}, state.account.profile);
      profile.date_of_birth = dateOfBirth;
      // We only want to commit the mutation if the update was successful
      await AccountsService.updateProfile(profile);
      commit(types.UPDATE_ACCOUNT, { profile: profile });
    },
    async update__account_password(
      { commit },
      { currentPassword, newPassword1, newPassword2 }
    ) {
      // We only want to commit the mutation if the update was successful
      await AccountsService.updatePassword(
        currentPassword,
        newPassword1,
        newPassword2
      );
      commit(types.UPDATE_ACCOUNT, { password: newPassword1 });
    },
  },
};
