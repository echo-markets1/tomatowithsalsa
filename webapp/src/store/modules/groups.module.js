import GroupsDataService from "@/services/groups-data.service";
import { GROUPS as types } from "@/store/mutation-types.js";
import { createLookupFromListByKey } from "@/store/create-lookup-from-list-by-key.js";
import authHeader from "@/services/auth-header";
import axiosWrapper from "@/services/axios-wrapper";
export const groups = {
  namespaced: true,
  state: {
    discoveredGroups: {
      // Groups the user has encountered while exploring in the session
    },
    userGroups: {
      // Groups the user is an active member of
    },
  },
  getters: {
    // Getters for specific groups
    userGroups__from_portfolioId: (state) => (portfolioId) => {
      return Object.fromEntries(
        Object.entries(state.userGroups).filter(([, group]) =>
          group.members
            .map((member) => member.portfolio_id)
            .includes(portfolioId)
        )
      );
    },
    userPortfolioId__from_groupId: (state, _, rootState) => (groupId) => {
      const userMember = state.userGroups[groupId].members.find((member) => {
        return member.user_id == rootState.accounts.userAccount.account.id;
      });
      return userMember.portfolio_id || null;
    },
    userGroupsIds(state) {
      return Object.keys(state.userGroups);
    },
  },
  mutations: {
    [types.LOAD_USER_GROUPS](state, groups) {
      if (groups.length > 0) {
        state.userGroups = createLookupFromListByKey(
          state.userGroups,
          groups,
          "id"
        );
      }
    },
    [types.ADD_USER_GROUP](state, group) {
      state.userGroups = createLookupFromListByKey(
        state.userGroups,
        [group],
        "id"
      );
    },
    [types.EDIT_USER_GROUP](state, group) {
      state.userGroups = createLookupFromListByKey(
        state.userGroups,
        [group],
        "id"
      );
    },
    [types.LOAD_DISCOVERED_GROUPS](state, groups) {
      state.discoveredGroups = createLookupFromListByKey(
        state.discoveredGroups,
        groups,
        "id"
      );
    },
    [types.ADD_DISCOVERED_GROUP](state, group) {
      state.discoveredGroups = createLookupFromListByKey(
        state.discoveredGroups,
        [group],
        "id"
      );
    },
    [types.REMOVE_DISCOVERED_GROUP](state, groupId) {
      delete state.discoveredGroups[groupId];
      state.discoveredGroups = Object.assign({}, state.discoveredGroups);
    },
    [types.LEAVE_USER_GROUP](state, groupId) {
      delete state.userGroups[groupId];
      state.userGroups = Object.assign({}, state.userGroups);
    },
  },
  actions: {
    async create__group(
      { commit },
      { name, description, privacy, portfolioId }
    ) {
      const response = await GroupsDataService.createGroup(
        name,
        description,
        privacy,
        portfolioId
      );
      const group = response.data;
      // Store before the group data gets augmented by the commit logic
      const groupId = group.id;
      commit(types.ADD_USER_GROUP, group);
      return groupId;
    },
    async edit__group({ commit }, { groupId, name, description }) {
      const response = await GroupsDataService.editGroup(
        groupId,
        name,
        description
      );
      const group = response.data;
      commit(types.ADD_USER_GROUP, group);
    },
    async add__discoveredGroup({ commit }, { groupId }) {
      const response = await GroupsDataService.getGroupById(groupId);
      const group = response.data;
      commit(types.ADD_DISCOVERED_GROUP, group);
    },
    /**
     * Checks the store to see if a given group has been loaded. If already
     * loaded, return the group from the store. Else call the BE API and add it
     * to the store.
     * @param {*} context
     * @param {Object} params - The unique identifier of the group being queried for
     * @returns - The group object for the given groupId
     */
    async get__group_by_id({ state, dispatch }, { groupId }) {
      let group =
        state.userGroups[groupId] || state.discoveredGroups[groupId] || null;
      if (group == null) {
        await dispatch("add__discoveredGroup", { groupId });
        group = state.discoveredGroups[groupId];
      }
      return group;
    },
    async join__group({ commit }, { groupId, portfolioId }) {
      const response = await GroupsDataService.addMember(
        groupId,
        null,
        portfolioId
      );
      const group = response.data;
      commit(types.ADD_USER_GROUP, group);
      commit(types.REMOVE_DISCOVERED_GROUP, groupId);
    },
    async leave__group({ rootState, commit }, { groupId }) {
      await GroupsDataService.deleteMember(
        groupId,
        rootState.accounts.userAccount.account.id
      );
      commit(types.LEAVE_USER_GROUP, groupId);
    },
    async add__member({ commit }, { groupId, memberId }) {
      const response = await GroupsDataService.addMember(
        groupId,
        memberId,
        null
      );
      const group = response.data;
      commit(types.EDIT_USER_GROUP, group);
    },
    async edit__member({ commit }, { groupId, memberId, type }) {
      const response = await GroupsDataService.editMember(
        groupId,
        memberId,
        type
      );
      const group = response.data;
      commit(types.ADD_USER_GROUP, group);
    },
    async delete__member({ commit }, { groupId, memberId }) {
      const response = await GroupsDataService.deleteMember(groupId, memberId);
      const group = response.data;
      commit(types.ADD_USER_GROUP, group);
    },
    async initialize({ commit }) {
      const response = await GroupsDataService.getUserGroups();
      commit(types.LOAD_USER_GROUPS, response.data.results);
      let next = response.data.next;
      while (next != null) {
        const nextResponse = await axiosWrapper.get(next, {
          headers: authHeader(),
        });
        commit(types.LOAD_USER_GROUPS, nextResponse.data.results);
        next = nextResponse.data.next;
      }
    },
  },
};
