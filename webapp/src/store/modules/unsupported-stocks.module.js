import { UNSUPPORTED_STOCKS as types } from "@/store/mutation-types.js";
/**
 * A module tracking stocks whose data is not supported by twelvedata. Critical
 * to distinguish between when we are waiting for data to load, and there is no
 * data to load.
 */
export const unsupportedStocks = {
  namespaced: true,
  state: {
    symbols: [],
  },
  mutations: {
    /**
     * Commits daily quote info to the store, keyed by symbol
     * @param {Object} state
     * @param {Object} quote - Object containing daily quote for a given symbol
     */
    [types.ADD_SYMBOL](state, symbol) {
      state.symbols.push(symbol);
    },
  },
  actions: {
    async add__unsupported_stock_symbol({ commit }, symbol) {
      console.log(`DEBUG: Adding '${symbol}' to unsupportedStocks list.`);
      commit(types.ADD_SYMBOL, symbol);
    },
  },
};
