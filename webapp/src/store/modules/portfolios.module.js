import PortfoliosService from "@/services/portfolios.service.js";
import { PORTFOLIOS as types } from "@/store/mutation-types.js";
import { userPortfolios } from "@/store/modules/portfolios/user-portfolios.module.js";
import { publicPortfolios } from "@/store/modules/portfolios/public-portfolios.module.js";
import { GRANULARITIES } from "@/constants/twelvedata-constants.js";

export const portfolios = {
  namespaced: true,
  modules: {
    userPortfolios,
    publicPortfolios,
  },
  getters: {
    userPortfolios(state) {
      return state.userPortfolios.portfolios;
    },
    publicPortfolios(state) {
      return state.publicPortfolios.portfolios;
    },
    isUserPortfolio: (state, getters) => (portfolioId) => {
      return Object.prototype.hasOwnProperty.call(
        getters.userPortfolios,
        portfolioId
      );
    },
  },
  mutations: {
    /**
     * Overwrites the balance_current of the portfolio. This is used to represent
     * the current value of all investments and cash in the portfolio.
     * @param {Object} state
     * @param {Object} params - Object containing the portfolioId and value to
     *  update balance_current
     */
    [types.SET_CURRENT_VALUE](state, { portfolioId, currentValue }) {
      let portfolio = state.userPortfolios.portfolios[portfolioId];
      if (portfolio) {
        state.userPortfolios.portfolios[portfolioId] = Object.assign(
          {},
          portfolio,
          {
            balance_current: currentValue,
          }
        );
      } else {
        portfolio = state.publicPortfolios.portfolios[portfolioId];
        state.publicPortfolios.portfolios[portfolioId] = Object.assign(
          {},
          portfolio,
          {
            balance_current: currentValue,
          }
        );
      }
    },
    /**
     * Updates the portfolio state with information about the total return of each investment
     * @param {Object} state
     * @param {Object} params - The unique identifier of the portfolio and the updated
     *  investment info.
     */
    [types.SET_INVESTMENTS_TOTAL_RETURNS](
      state,
      { portfolioId, updatedInvestments }
    ) {
      let portfolio = state.userPortfolios.portfolios[portfolioId];
      if (portfolio) {
        // If this is one of the user's portfolios, update userPortfolios
        state.userPortfolios.portfolios[portfolioId] = Object.assign(
          {},
          portfolio,
          {
            investments: updatedInvestments,
          }
        );
      } else {
        // else update publicPortfolios
        portfolio = state.publicPortfolios.portfolios[portfolioId];
        state.publicPortfolios.portfolios[portfolioId] = Object.assign(
          {},
          portfolio,
          {
            investments: updatedInvestments,
          }
        );
      }
    },
  },
  actions: {
    /**
     * Finds the most recent timeseriesDatapoint and uses that to update the
     * portfolios balance_current
     * @param {Object} context
     * @param {String} portfolioId - The unique identifier of the portfolio
     */
    async update_portfolio_balance_current__from_id(
      { commit, rootGetters },
      portfolioId
    ) {
      const currentValue = rootGetters[
        "portfolioHistory/reverse_chronological_timeseries__from_id_granularity"
      ]({
        id: portfolioId,
        granularity: GRANULARITIES.FIVE_MIN,
      })[0 /*The most recent timeseriesDatapoint*/][1 /*The value at that datapoint */];
      commit(types.SET_CURRENT_VALUE, { portfolioId, currentValue });
    },
    /**
     * Calculates the current_value, total_return, and total_return_percent for
     * every investment in the portfolio, based on the most recent value of each
     * stock, and commits that info to the store.
     * @param {*} context
     * @param {String} portfolioId - The unique identifier of the portfolio
     */
    async update_portfolio_investment_returns__from_id(
      { state, commit, rootGetters },
      portfolioId
    ) {
      const portfolio =
        state.userPortfolios.portfolios[portfolioId] ||
        state.publicPortfolios.portfolios[portfolioId];

      const symbolMostRecentTimeseriesDatapointLookup =
        rootGetters["stockHistory/most_recent_values"];
      /*
       Create a new array of investments augmented with current value and return
       data by leveraging the most recent values of each related stock
      */
      let updatedInvestments = portfolio.investments.map((investment) => {
        // Avoid mutating the state by copying to a new object
        const updatedInvestment = Object.assign({}, investment);
        if (
          investment.ticker_symbol in symbolMostRecentTimeseriesDatapointLookup
        ) {
          // Handle non cash investments
          updatedInvestment["current_value"] =
            parseFloat(investment.quantity) *
            parseFloat(
              symbolMostRecentTimeseriesDatapointLookup[
              investment.ticker_symbol
              ][1]
            );
        } else {
          /*
           If twelvedata does not support a symbol, it will not show up in the lookup.
           Default to the provided institution_value
          */
          updatedInvestment["current_value"] = parseFloat(
            investment.institution_value
          );
        }
        updatedInvestment["total_return"] =
          parseFloat(updatedInvestment.current_value) -
          parseFloat(investment.cost_basis);
        updatedInvestment["total_return_percent"] =
          (parseFloat(updatedInvestment.total_return) /
            parseFloat(investment.cost_basis)) *
          100;
        return updatedInvestment;
      });
      // Sorted by percent return highest to lowest
      updatedInvestments.sort(
        (a, b) =>
          parseFloat(b.total_return_percent) -
          parseFloat(a.total_return_percent)
      );
      commit(types.SET_INVESTMENTS_TOTAL_RETURNS, {
        portfolioId,
        updatedInvestments,
      });
    },
    async get({ state, commit, dispatch }, portfolioId) {
      // TODO before return, calculate percentages of each investment in the portfolio
      let portfolio =
        state.userPortfolios.portfolios[portfolioId] ||
        state.publicPortfolios.portfolios[portfolioId];
      if (portfolio) {
        return portfolio;
      } else {
        try {
          /*
           * All user portfolios will loaded into store on application load, or
           * on account linkage. Therefore any portfolio that requires querying
           * the BE will be a public portfolio
           */
          const response = await PortfoliosService.get(portfolioId);
          portfolio = response.data;
          commit(`publicPortfolios/${types.ADD_PORTFOLIO}`, portfolio);
          await dispatch(
            "portfolioHistory/refresh_portfolio_history",
            {
              portfolioId,
              granularity: GRANULARITIES.FIVE_MIN,
            },
            { root: true }
          );
          return portfolio;
        } catch (e) {
          console.log(
            `Could not find portfolio with id: ${portfolioId}. Returning an empty portfolio. ${e}`
          );
          // Return an "empty" portfolio if none are found
          return {
            institution: {},
            investments: [],
            transactions: [],
          };
        }
      }
    },
    async getUserPortfolios({ state, commit, dispatch }, userId) {
      // TODO before return, calculate percentages of each investment in the portfolio
      try {
        /*
         * All user portfolios will loaded into store on application load, or
         * on account linkage. Therefore any portfolio that requires querying
         * the BE will be a public portfolio
         */
        const response = await PortfoliosService.getPortfolioByUserId(userId);
        const portfolios = response.data;

        portfolios.forEach(async (portfolio) => {
          let user_portfolio = state.userPortfolios.portfolios[portfolio.id];
          if (!user_portfolio) {
            let public_portfolio =
              state.publicPortfolios.portfolios[portfolio.id];
            if (!public_portfolio) {
              commit(`publicPortfolios/${types.ADD_PORTFOLIO}`, portfolio);
              await dispatch(
                "portfolioHistory/refresh_portfolio_history",
                {
                  portfolioId: portfolio.id,
                  granularity: GRANULARITIES.FIVE_MIN,
                },
                { root: true }
              );
            }
          }
        });
        return portfolios;
      } catch (e) {
        console.log(
          `Could not find portfolios for user with id: ${userId}. Returning an empty portfolio. ${e}`
        );
        // Return an "empty" portfolio if none are found
        return {
          institution: {},
          investments: [],
          transactions: [],
        };
      }
    },
    async getGroupPortfolios({ state, commit, dispatch }, groupId) {
      /// TODO before return, calculate percentages of each investment in the portfolio
      // Check if user portfolio

      let group_portfolios = await PortfoliosService.getGroupPortfolios(
        groupId
      );

      group_portfolios.data.forEach(async (portfolio) => {
        let user_portfolio = state.userPortfolios.portfolios[portfolio.id];
        if (!user_portfolio) {
          let public_portfolio =
            state.publicPortfolios.portfolios[portfolio.id];
          if (!public_portfolio) {
            commit(`publicPortfolios/${types.ADD_PORTFOLIO}`, portfolio);
            await dispatch(
              "portfolioHistory/refresh_portfolio_history",
              {
                portfolioId: portfolio.id,
                granularity: GRANULARITIES.FIVE_MIN,
              },
              { root: true }
            );
          }
        }
      });
      return group_portfolios;
    },

    async initialize({ dispatch }) {
      return dispatch("userPortfolios/initialize");
    },
  },
};
