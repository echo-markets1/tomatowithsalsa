import { TRANSACTIONS as types } from "@/store/mutation-types.js";
import TransactionsService from "@/services/transactions.service";
import { createLookupFromListByKey } from "@/store/create-lookup-from-list-by-key.js";
import { cloneDeep } from "lodash";
export const transactions = {
  namespaced: true,
  state: {
    allTransactions: {},
  },
  mutations: {
    [types.LOAD_TRANSACTIONS](state, transactions) {
      state.allTransactions = createLookupFromListByKey(
        state.allTransactions,
        transactions,
        "id"
      );
    },
    [types.TOGGLE_LIKE_TRANSACTION](state, transaction) {
      state.allTransactions[transaction.id] = Object.assign(
        {},
        state.allTransactions[transaction.id],
        transaction
      );
    },
    [types.UPDATE_TRANSACTION_REPLIES_COUNT](
      state,
      { transactionId, repliesCount }
    ) {
      state.allTransactions[transactionId] = Object.assign(
        {},
        state.allTransactions[transactionId],
        { replies_count: repliesCount }
      );
    },
  },
  actions: {
    async getTransactions({ commit }, { groupId = null, portfolioId = null }) {
      const response = await TransactionsService.get({ groupId, portfolioId });
      // The transactions will be modified during the mutation, create a deep copy
      // to return the unmodified version
      const transactions = cloneDeep(response.data.results);
      commit(types.LOAD_TRANSACTIONS, transactions);
      return response;
    },
    async like({ commit }, transactionId) {
      const response = await TransactionsService.like(transactionId);
      commit(types.TOGGLE_LIKE_TRANSACTION, response.data);
    },
  },
};
