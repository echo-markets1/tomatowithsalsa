import StockHistoryService from "@/services/market-data/stock-history.service";
import { timeseries } from "@/store/modules/historical-data/timeseries.module.js";
import { TIMESERIES as types, ADD_SYMBOL } from "@/store/mutation-types";
import { GRANULARITIES } from "@/constants/twelvedata-constants.js";
import { shouldRefreshTimeseries } from "@/store/timeseries-refresh-checker.js";

/**
 * A module for historical data associated with specific stocks
 */
export const stockHistory = {
  namespaced: true,
  modules: {
    timeseries,
  },
  getters: {
    /**
     * Identifies the most recent value of all stocks with 5min timeseries
     * @param {*} state
     * @param {*} getters
     * @returns {Object} - A mapping of ticker symbols to their most recent value
     */
    most_recent_values(state, getters) {
      return Object.entries(state.timeseries).reduce(function(
        symbolValueLookup,
        [symbol]
      ) {
        symbolValueLookup[
          symbol
        ] = getters.reverse_chronological_timeseries__from_id_granularity({
          id: symbol,
          granularity: GRANULARITIES.FIVE_MIN, // Should be the smallest that is supported, currently 5min.
        })[0]; // Index 0 should be the most recent datapoint
        return symbolValueLookup;
      },
      {});
    },
    /**
     * Finds and returns the largest collection of timestamps from the timeseries across a list
     * of stocks. This is of particular use when building composite timeseries,
     * such as for the portfolio, because the number of datapoints twelvedata is
     * able to return for a given stocks timeseries often varies, and this data
     * can be leveraged to help bridge the gap.
     * @param {*} state
     * @param {Object} params - The list of ticker symbols and the granularity of
     *  the timeseries to inspect
     * @returns {Array} - The list of timestamps
     */
    timestamp_range__from_symbols_and_granularity: (state) => ({
      symbolList,
      granularity,
    }) => {
      let max = 0;
      return Object.entries(state.timeseries)
        .filter(([symbol]) => symbolList.includes(symbol))
        .reduce(function(timestamps, [, timeseriesByGranularity]) {
          const fiveMinTimestamps = Object.keys(
            timeseriesByGranularity[granularity]
          );
          if (max < fiveMinTimestamps.length) {
            max = fiveMinTimestamps.length;
            timestamps = fiveMinTimestamps;
          }
          return timestamps;
        }, []);
    },
  },
  actions: {
    /**
     * Updates the timeseries data for list of stocks on a specific time interval
     * @param {*} context
     * @param {*} params - The list of ticker symbols and granularity for which
     *  to refresh the data
     */
    async refresh_multiple_symbols_data(
      { dispatch },
      { symbols, granularity }
    ) {

      const response = await StockHistoryService.getTimeseriesData(
        symbols,
        granularity
      );

      const promises = [];
      for (const [symbol, stockData] of Object.entries(response.data)) {
        promises.push(
          dispatch("_process_twelve_data_response", {
            symbol,
            granularity,
            apiResponseData: stockData,
          })
        );
      }
      await Promise.all(promises);
    },
    /**
     * Updates the timeseries data for a single stock on a specific time interval.
     * Maintain as a separate action from refresh_multiple_symbols_data
     * because twelvedata provides a different response for 1 vs many symbols.
     * @param {*} context
     * @param {*} params - The ticker symbols and granularity for which to refresh
     *  the data
     */
    async refresh_single_symbol_data(
      { dispatch, getters },
      { symbol, granularity }
    ) {
      let lastTimestamp = 0;
      try {
        lastTimestamp = getters.reverse_chronological_timeseries__from_id_granularity(
          {
            id: symbol,
            granularity,
          }
        )[0 /*The most recent timeseriesDatapoint*/][0 /*The timestamp at that datapoint */];
      } catch {
        // pass
      }
      if (
        shouldRefreshTimeseries(
          granularity,
          // Get the timestamp or default to 0, since 0 will always trigger a refresh
          lastTimestamp,
          new Date()
        )
      ) {
        console.log(
          `Refreshing timeseries data for stock: ${symbol} and granularity: ${granularity}`
        );
        const response = await StockHistoryService.getTimeseriesData(
          [symbol],
          granularity
        );
        await dispatch("_process_twelve_data_response", {
          symbol,
          granularity,
          apiResponseData: response.data,
        });
      } else {
        console.log(
          `Timeseries data for stock: ${symbol} and granularity: ${granularity} is up to date`
        );
      }
    },
    /**
     * A "private" action meant to abstract to logic of processing timeseries data
     * from the api response, and commiting the result.
     * @param {*} context
     * @param {*} params - The ticker symbol and granularity of the timeseries,
     *  and the response from twelvedata
     */
    async _process_twelve_data_response(
      { commit },
      { symbol, granularity, apiResponseData }
    ) {
      if (apiResponseData.status != "error") {
        const timeseriesData = apiResponseData.values.reduce(function(
          timeseriesData,
          datapoint
        ) {
          const timestamp = new Date(datapoint.datetime).getTime();
          timeseriesData.push([timestamp, parseFloat(datapoint.close)]);
          return timeseriesData;
        },
        []);
        commit(types.ADD_TIMESERIES_DATA, {
          id: symbol,
          timeseriesData: timeseriesData,
          granularity: granularity,
        });
      } else {
        console.log(
          `WARN: twelvedata threw an error while retrieving timeseries data for symbol: ${symbol}`
        );
        commit(`unsupportedStocks/${ADD_SYMBOL}`, symbol, {
          root: true,
        });
      }
    },
  },
};
