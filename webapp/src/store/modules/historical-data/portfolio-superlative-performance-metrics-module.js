import SuperlativePerformanceMetricsService from "@/services/superlative-performance-metrics.service.js";
import { SUPERLATIVE_PERFORMANCE_METRICS as types } from "@/store/mutation-types.js";
import { createLookupFromListByKey } from "@/store/create-lookup-from-list-by-key.js";

function _createLookupByPeriodFromSuperlativeMetricsList(
  portfolioId,
  type,
  metricsPayload
) {
  return createLookupFromListByKey(
    {},
    metricsPayload[portfolioId][type],
    "period"
  );
}
export const portfolioSuperlativePerformanceMetrics = {
  namespaced: true,
  modules: {},
  mutations: {
    [types.LOAD_WORST_PERFORMANCE_METRICS](
      state,
      { portfolioId, metricsPayload }
    ) {
      /*
       Accepts a payload of a list of best performance metrics, keyed by
       portfolioId and type (ie: WORST), and converts the list to also be keyed
       by period.

       Example payload:
       "03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw": {
           "WORST": [
                {
                    "percent_return": -10.197,
                    "period": "1d",
                    "start_date": "2021-08-15",
                    "end_date": "2021-08-16"
                },
                {
                    "percent_return": 0.635,
                    "period": "1m",
                    "start_date": "2021-07-17",
                    "end_date": "2021-08-16"
                },
                {
                    "percent_return": -9.984,
                    "period": "1w",
                    "start_date": "2021-08-09",
                    "end_date": "2021-08-16"
                }
            ]
       }
       
       Example output written to store:
       "03vY12a9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw": {
            "WORST": {
                "1d": {
                    "percent_return": -10.197,
                    "start_date": "2021-08-15",
                    "end_date": "2021-08-16"
                },
                "1w": {
                    "percent_return": 0.635,
                    "start_date": "2021-07-17",
                    "end_date": "2021-08-16"
                },
                "1m": {
                    "percent_return": -9.984,
                    "start_date": "2021-08-09",
                    "end_date": "2021-08-16"
                }
            },
        }
    */
      const type = "WORST";
      const metricsByPeriod = _createLookupByPeriodFromSuperlativeMetricsList(
        portfolioId,
        type,
        metricsPayload
      );
      state[portfolioId] = Object.assign({}, state[portfolioId], {
        [type]: metricsByPeriod,
      });
    },
    [types.LOAD_BEST_PERFORMANCE_METRICS](
      state,
      { portfolioId, metricsPayload }
    ) {
      /*
       Accepts a payload of a list of best performance metrics, keyed by
       portfolioId and type (ie: BEST), and converts the list to also be keyed
       by period.

       Example payload:
       "03vYjra9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw": {
           "BEST": [
                {
                    "percent_return": -10.197,
                    "period": "1d",
                    "start_date": "2021-08-15",
                    "end_date": "2021-08-16"
                },
                {
                    "percent_return": 0.635,
                    "period": "1m",
                    "start_date": "2021-07-17",
                    "end_date": "2021-08-16"
                },
                {
                    "percent_return": -9.984,
                    "period": "1w",
                    "start_date": "2021-08-09",
                    "end_date": "2021-08-16"
                }
            ]
       }
       
       Example output written to store:
       "03vY12a9rpH45dV9vmAbtxnn1jZ5bYHe5w7kw": {
            "BEST": {
                "1d": {
                    "percent_return": -10.197,
                    "start_date": "2021-08-15",
                    "end_date": "2021-08-16"
                },
                "1w": {
                    "percent_return": 0.635,
                    "start_date": "2021-07-17",
                    "end_date": "2021-08-16"
                },
                "1m": {
                    "percent_return": -9.984,
                    "start_date": "2021-08-09",
                    "end_date": "2021-08-16"
                }
            },
        }
    */
      const type = "BEST";
      const metricsByPeriod = _createLookupByPeriodFromSuperlativeMetricsList(
        portfolioId,
        type,
        metricsPayload
      );
      state[portfolioId] = Object.assign({}, state[portfolioId], {
        [type]: metricsByPeriod,
      });
    },
  },
  actions: {
    // To be called once the userPortfolios have been initialized
    async load_single_portfolio__by_id({ dispatch }, portfolioId) {
      const promises = [];
      // Asynchronously retrieve the metrics for the given portfolio by it's id
      promises.push(
        dispatch("load_single_portfolio_BEST_metrics__by_id", portfolioId),
        dispatch("load_single_portfolio_WORST_metrics__by_id", portfolioId)
      );
      return Promise.all(promises);
    },

    async load_single_portfolio_BEST_metrics__by_id({ commit }, portfolioId) {
      const response = await SuperlativePerformanceMetricsService.getBestDWMMetrics(
        portfolioId
      );
      commit(types.LOAD_BEST_PERFORMANCE_METRICS, {
        portfolioId,
        metricsPayload: response.data,
      });
    },
    async load_single_portfolio_WORST_metrics__by_id({ commit }, portfolioId) {
      const response = await SuperlativePerformanceMetricsService.getWorstDWMMetrics(
        portfolioId
      );
      commit(types.LOAD_WORST_PERFORMANCE_METRICS, {
        portfolioId,
        metricsPayload: response.data,
      });
    },
  },
};
