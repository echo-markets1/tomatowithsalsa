import PortfolioHistoryService from "@/services/portfolio-history.service";
import { shouldRefreshTimeseries } from "@/store/timeseries-refresh-checker.js";
import { timeseries } from "@/store/modules/historical-data/timeseries.module.js";
import { TIMESERIES as types } from "@/store/mutation-types";
import { GRANULARITIES } from "@/constants/twelvedata-constants.js";

export const portfolioHistory = {
  namespaced: true,
  modules: {
    timeseries,
  },
  actions: {
    /**
     * Triggers a call to the HOTH service to retrieve pre-calculated portfolio timeseries
     * data for the gven portfolio and granularity.
     * @param {*} context
     * @param {Object} params - The unique identifier of the portfolio and the granularity
     *  for which to retrieve timeseries data for.
     */
    async get_past_days_data({ commit }, { portfolioId, granularity }) {
      const response = await PortfolioHistoryService.getPortfolioTimeseriesData(
        portfolioId,
        granularity
      );
      const timeseriesData = response.data.map((i) => [
        i.unix_timestamp * 1000, // Convert to milliseconds for js compatibility
        parseFloat(i.value),
      ]);
      commit(types.ADD_TIMESERIES_DATA, {
        id: portfolioId,
        timeseriesData,
        granularity,
      });
    },
    /**
     * Dispatches a call to the stockHistory module which will call twelveData
     * and use the response to update it's store of stock timeseries data. Once
     * that call has completed, loops through all the investments of the given
     * portfolio, and compiles timeseriesData based on the stock timeseries and
     * the holdings informatin about each investment.
     *
     * @param {*} context
     * @param {Object} params - The unique identifier of the portfolio and
     *  the granularity for which to retrieve timeseries data for.
     */
    async get_intra_day_data(
      { rootState, commit, dispatch, rootGetters },
      { portfolioId, granularity }
    ) {
      const portfolio = await dispatch("portfolios/get", portfolioId, {
        root: true,
      });
      const investments = portfolio.investments;
      const symbols = investments.reduce(function(symbols, investment) {
        if (investment.security_type != "cash") {
          symbols.push(investment.ticker_symbol);
        }
        return symbols;
      }, []);

      await dispatch(
        "stockHistory/refresh_multiple_symbols_data",
        { symbols, granularity },
        { root: true }
      );

      const timeseriesToAdd = {
        // timestamp: value
      };

      // The longest list of all the timestamps across the timeseries of the investments
      const timestamps = rootGetters[
        "stockHistory/timestamp_range__from_symbols_and_granularity"
      ]({ symbolList: symbols, granularity });

      /*
       * For each investment, create a new timeseries based on the stock timeseries data,
       * and the size of the investment, to estimate the value of the portfolio at that moment
       * This means looping through every timestep for every stock in the portfolio,
       * and keeping a running total of the value at each timestep by storing and
       * updating timestamp value pairs in an object
       */
      investments
        // Ignore cash investments
        .filter((investment) => investment.security_type != "cash")
        .forEach((investment) => {
          // Retrieve the timeseries for the stock corresponding to each investment
          let stockTimeseries = {};
          try {
            stockTimeseries =
              rootState.stockHistory.timeseries[investment.ticker_symbol][
                granularity
              ];
          } catch {
            /* 
              If an error occured retrieving a timeseries data for a particular stock
              default to the plaid provided institution price. 
            */
            stockTimeseries[timestamps[0]] = parseFloat(
              investment.institution_price
            );
          }
          /*
           * Identify the most recent timestamp that the timeseries is gauranteed
           * to have data for, defaulting to the earliest timestamp
           */
          let mostRecentTimestampWithData = Object.keys(stockTimeseries).sort(
            (a, b) => parseFloat(a) - parseFloat(b)
          )[0];
          /*
           * twelvedata, and therfore our stock timeseries data, is sometimes inconsistent
           * across symbols/stocks. Some stocks seem to be missing data points in their
           * responses, which can lead to those stocks essentially becoming unaccounted
           * for the in the portfolio timeseries calculation at the missing timestamp,
           * creating the misleading appearance of large variance in value. As an
           * attempt to mitigate the data quality, if we recognize that a datapoint is
           * missing for a specifiic timestamp, we will default back to the datapoint
           * the timestamp closest in time to the missing data point that we can confirm
           * we have data for.
           */
          timestamps.forEach((timestamp) => {
            let stockValueAtTimestamp = stockTimeseries[timestamp];
            const portfolioValueAtTimestamp =
              timeseriesToAdd[timestamp] || portfolio.cash;
            if (stockValueAtTimestamp) {
              mostRecentTimestampWithData = timestamp;
            } else {
              /*
               * If there was no datapoint at the current timestamp, default to the
               * last known datapoint
               */
              stockValueAtTimestamp =
                stockTimeseries[mostRecentTimestampWithData];
            }
            timeseriesToAdd[timestamp] =
              portfolioValueAtTimestamp +
              parseFloat(stockValueAtTimestamp) *
                parseFloat(investment.quantity);
          });
        });

      commit(types.ADD_TIMESERIES_DATA, {
        id: portfolioId,
        timeseriesData: Object.entries(timeseriesToAdd),
        granularity,
      });
    },

    /**
     * Checks to see if portfolio timeseries data needs refreshing, either by
     * updating the timeseries for a given stock in the store, or by making a
     * call to our BE of stored Portfolio Timeseries data.
     * @param {*} context
     * @param {Object} params - The unique identifier of the portfolio and
     *  the granularity for which to retrieve timeseries data for.
     * @returns {[Promise]} - An array of promises triggered by this function.
     */
    async refresh_portfolio_history(
      { dispatch, getters },
      { portfolioId, granularity }
    ) {
      const portfolioTimeseries = getters.reverse_chronological_timeseries__from_id_granularity(
        { id: portfolioId, granularity }
      );
      const promises = [];
      // Get the the most recent datapoint, or default to an empty array
      const mostRecentTimeseriesDatapoint = portfolioTimeseries[0] || [];
      if (
        shouldRefreshTimeseries(
          granularity,
          // Get the timestamp or default to 0, since 0 will always trigger a refresh
          mostRecentTimeseriesDatapoint[0] || 0,
          new Date()
        )
      ) {
        const pastDaysGranularities = [
          GRANULARITIES.ONE_DAY,
          GRANULARITIES.ONE_WEEK,
        ];
        const intraDayGranularities = [
          GRANULARITIES.FIVE_MIN,
          GRANULARITIES.ONE_HOUR,
        ];
        if (pastDaysGranularities.includes(granularity)) {
          console.log(
            `Refreshing previous days timeseries data for portfolio: ${portfolioId} and granularity: ${granularity}`
          );
          await dispatch("get_past_days_data", {
            portfolioId,
            granularity,
          });
        }
        if (intraDayGranularities.includes(granularity)) {
          console.log(
            `Refreshing intraday timeseries data for portfolio: ${portfolioId} and granularity: ${granularity}`
          );
          await dispatch("get_intra_day_data", {
            portfolioId,
            granularity,
          });
          if (granularity == GRANULARITIES.FIVE_MIN) {
            promises.push(
              dispatch(
                "portfolios/update_portfolio_balance_current__from_id",
                portfolioId,
                { root: true }
              )
            );
            promises.push(
              dispatch(
                "portfolios/update_portfolio_investment_returns__from_id",
                portfolioId,
                { root: true }
              )
            );
          }
        }
      } else {
        console.log(
          `Timeseries data for portfolio: ${portfolioId} and granularity: ${granularity} is up to date`
        );
      }
      return Promise.all(promises);
    },
  },
};
