import CompositeHistoryService from "@/services/composite-history.service";
import PortfoliosService from "@/services/portfolios.service";
import { shouldRefreshTimeseries } from "@/store/timeseries-refresh-checker.js";
import { timeseries } from "@/store/modules/historical-data/timeseries.module.js";
import { TIMESERIES as types } from "@/store/mutation-types";
import { GRANULARITIES } from "@/constants/twelvedata-constants.js";

export const compositeHistory = {
  namespaced: true,
  modules: {
    timeseries,
  },
  actions: {
    /**
     * Triggers a call to the HOTH service to retrieve pre-calculated user timeseries
     * data for the gven user and granularity.
     * @param {*} context
     * @param {Object} params - The userId and the granularity for which to retrieve
     *  timeseries data for.
     */
    async get_past_days_data({ commit }, { userId, granularity }) {
      const response = await CompositeHistoryService.getCompositeTimeseriesData(
        userId,
        granularity
      );
      const timeseriesData = response.data.map((i) => [
        i.unix_timestamp * 1000, // Convert to milliseconds for js compatibility
        parseFloat(i.value),
      ]);
      commit(types.ADD_TIMESERIES_DATA, {
        id: userId,
        timeseriesData,
        granularity,
      });
    },

    /**
     * Dispatches a call to the stockHistory module which will call twelveData
     * and use the response to update it's store of stock timeseries data. Once
     * that call has completed, loops through all the investments of the given
     * portfolio, and compiles timeseriesData based on the stock timeseries and
     * the holdings information about each investment.
     *
     * @param {*} context
     * @param {Object} params - The unique identifier of the portfolio and
     *  the granularity for which to retrieve timeseries data for.
     */
    async get_intra_day_data(
      { rootState, commit, dispatch, rootGetters },
      { userId, granularity }
    ) {
      const promises = [];
      let portfolioIds = [];
      if (userId == rootState.accounts.userAccount.account.id) {
        portfolioIds =
          rootGetters["portfolios/userPortfolios/all_portfolioIds"];
      } else {
        const response = await PortfoliosService.getOtherUserPortfolios(userId);
        portfolioIds = response.data.reduce(function(
          allPortfolioIds,
          currentPortfolio
        ) {
          allPortfolioIds.push(currentPortfolio["id"]);
          return allPortfolioIds;
        },
        []);
      }
      const symbols = [];
      const compositeInvestments = {};
      let cashTotal = 0;
      for (const portfolioId of portfolioIds) {
        const portfolio = await dispatch("portfolios/get", portfolioId, {
          root: true,
        });
        cashTotal = cashTotal + portfolio.cash;
        const investments = portfolio.investments;
        investments.forEach((investment) => {
          const investmentHasTwelveDataSupport =
            (investment.security_type !== "cash" ||
              investment.security_subtype === "crypto") &&
            // twelvedata doesn't support derivatives/options
            investment.security_subtype !== "derivative";
          if (investmentHasTwelveDataSupport) {
            const symbol = investment.ticker_symbol;
            symbols.push(symbol);
            if (compositeInvestments[symbol] == null) {
              compositeInvestments[symbol] = {
                institution_price: parseFloat(investment["institution_price"]),
                quantity: parseFloat(investment["quantity"]),
              };
            } else {
              compositeInvestments[symbol]["quantity"] =
                compositeInvestments[symbol]["quantity"] +
                parseFloat(investment["quantity"]);
            }
          }
        });
        if (granularity == GRANULARITIES.FIVE_MIN) {
          promises.push(
            dispatch(
              "portfolios/update_portfolio_balance_current__from_id",
              portfolioId,
              { root: true }
            )
          );
          promises.push(
            dispatch(
              "portfolios/update_portfolio_investment_returns__from_id",
              portfolioId,
              { root: true }
            )
          );
        }
      }

      await dispatch(
        "stockHistory/refresh_multiple_symbols_data",
        { symbols, granularity },
        { root: true }
      );

      const timeseriesToAdd = {
        // timestamp: value
      };

      // The longest list of all the timestamps across the timeseries of the investments
      const timestamps = rootGetters[
        "stockHistory/timestamp_range__from_symbols_and_granularity"
      ]({ symbolList: symbols, granularity });

      /*
       * For each investment, create a new timeseries based on the stock timeseries data,
       * and the size of the investment, to estimate the value of the portfolio at that moment
       * This means looping through every timestep for every stock in the portfolio,
       * and keeping a running total of the value at each timestep by storing and
       * updating timestamp value pairs in an object
       */
      Object.entries(compositeInvestments).forEach(([symbol, investment]) => {
        // Retrieve the timeseries for the stock corresponding to each investment
        let stockTimeseries = {};
        try {
          stockTimeseries =
            rootState.stockHistory.timeseries[symbol][granularity];
        } catch {
          /* 
              If an error occured retrieving a timeseries data for a particular stock
              default to the plaid provided institution price. 
            */
          stockTimeseries[timestamps[0]] = parseFloat(
            investment.institution_price
          );
        }
        /*
         * Identify the most recent timestamp that the timeseries is gauranteed
         * to have data for, defaulting to the earliest timestamp
         */
        let mostRecentTimestampWithData = Object.keys(stockTimeseries).sort(
          (a, b) => parseFloat(a) - parseFloat(b)
        )[0];
        /*
         * twelvedata, and therfore our stock timeseries data, is sometimes inconsistent
         * across symbols/stocks. Some stocks seem to be missing data points in their
         * responses, which can lead to those stocks essentially becoming unaccounted
         * for the in the portfolio timeseries calculation at the missing timestamp,
         * creating the misleading appearance of large variance in value. As an
         * attempt to mitigate the data quality, if we recognize that a datapoint is
         * missing for a specifiic timestamp, we will default back to the datapoint
         * the timestamp closest in time to the missing data point that we can confirm
         * we have data for.
         */
        timestamps.forEach((timestamp) => {
          let stockValueAtTimestamp = stockTimeseries[timestamp];
          const portfolioValueAtTimestamp =
            timeseriesToAdd[timestamp] || cashTotal;
          if (stockValueAtTimestamp) {
            mostRecentTimestampWithData = timestamp;
          } else {
            /*
             * If there was no datapoint at the current timestamp, default to the
             * last known datapoint
             */
            stockValueAtTimestamp =
              stockTimeseries[mostRecentTimestampWithData];
          }
          timeseriesToAdd[timestamp] =
            portfolioValueAtTimestamp +
            parseFloat(stockValueAtTimestamp) * parseFloat(investment.quantity);
        });
      });

      commit(types.ADD_TIMESERIES_DATA, {
        id: userId,
        timeseriesData: Object.entries(timeseriesToAdd),
        granularity,
      });
      return Promise.all(promises);
    },

    /**
     * Checks to see if user timeseries data needs refreshing, either by
     * updating the timeseries for a given stock in the store, or by making a
     * call to our BE of stored Portfolio Timeseries data.
     * @param {*} context
     * @param {Object} params - The unique identifier of the user and
     *  the granularity for which to retrieve timeseries data for.
     * @returns {[Promise]} - An array of promises triggered by this function.
     */
    async refresh_composite_history(
      { dispatch, getters },
      { userId, granularity }
    ) {
      const compositeTimeseries = getters.reverse_chronological_timeseries__from_id_granularity(
        {
          id: userId,
          granularity,
        }
      );
      // Get the the most recent datapoint, or default to an empty array
      const mostRecentTimeseriesDatapoint = compositeTimeseries[0] || [];
      if (
        shouldRefreshTimeseries(
          granularity,
          // Get the timestamp or default to 0, since 0 will always trigger a refresh
          mostRecentTimeseriesDatapoint[0] || 0,
          new Date()
        )
      ) {
        const pastDaysGranularities = [
          GRANULARITIES.ONE_DAY,
          GRANULARITIES.ONE_WEEK,
        ];
        const intraDayGranularities = [
          GRANULARITIES.FIVE_MIN,
          GRANULARITIES.ONE_HOUR,
        ];
        if (pastDaysGranularities.includes(granularity)) {
          console.log(
            `Refreshing previous days timeseries data for user: ${userId} and granularity: ${granularity}`
          );
          await dispatch("get_past_days_data", {
            userId,
            granularity,
          });
        }
        if (intraDayGranularities.includes(granularity)) {
          console.log(
            `Refreshing intraday timeseries data for user: ${userId} and granularity: ${granularity}`
          );
          await dispatch("get_intra_day_data", {
            userId,
            granularity,
          });
        }
      } else {
        console.log(
          `Timeseries data for user: ${userId} and granularity: ${granularity} is up to date`
        );
      }
    },
  },
};
