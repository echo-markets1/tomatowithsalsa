import { TIMESERIES as types } from "@/store/mutation-types";
import Vue from "vue";
import { GRANULARITIES } from "@/constants/twelvedata-constants.js";
import { getTimezoneOffsetMS } from "@/dateUtils.js";

/**
 * A common timeseries module to abstract shared logic for handling timeseries data
 */
export const timeseries = {
  namespaced: false,
  // State must be a function in order to be reusable across multiple modules
  state: () => ({
    /*
     * id: {
     *   granularity: {
     *     timestamp: value,
     *   }
     * }
     */
  }),
  getters: {
    /**
     * Converts a timeseries object into the 2D array form [...[timestamp, value]]
     * and returns it sorted from most to least recent timestamp. The timestamps
     * are normalized against system time using the timezone offset.
     * @param {*} state
     * @param {Object} params - The unique identifier (ie: portfolioId, ticker symbol)
     *  and granularity to uniquely identify the timeseries
     * @returns {Array} - The timeseries as a 2D array
     */
    reverse_chronological_timeseries__from_id_granularity: (state) => ({
      id,
      granularity,
    }) => {
      /*
       * Using a JS Map object would be nice to preserve the typing of the keys,
       * but Vue 2.x does not support proper reactivity for Map objects. As a
       * hack around, map the values in the object back to floats.
       */
      const fromId = state[id] || {};
      const fromIdAndGranularity = fromId[granularity] || {};
      return Object.entries(fromIdAndGranularity)
        .map((entry) => [
          parseFloat(entry[0]) - getTimezoneOffsetMS(),
          parseFloat(entry[1]),
        ])
        .sort((a, b) => b[0] - a[0]);
    },
    /**
     * Leverages the timeseries data to provide the change in value of a stock
     * over a given day by comparing the first and most recent values in the timeseries.
     * @param {*} getters
     * @param {Object} id - The unique identifier (ie: portfolioId, ticker symbol)
     *  of the entity
     * @returns {Object} - A object containing data about the total and percent change in value,
     *  as well as the open and current price data
     */
    daily_return_data__from_id: (_, getters) => (id) => {
      const returnData = {
        open: 0,
        current: 0,
        total: 0,
        percent: 0,
        lastUpdated: 0,
      };
      const timeseries = getters.reverse_chronological_timeseries__from_id_granularity(
        { id, granularity: GRANULARITIES.FIVE_MIN }
      );
      if (timeseries.length > 0) {
        const currentDatapoint =
          timeseries[0 /*The most recent timeseriesDatapoint*/];
        const earliestDatapoint =
          timeseries[timeseries.length - 1 /*The earlest timeseriesDatapoint*/];
        const currentValue =
          currentDatapoint[1 /*The value at that datapoint */];
        const openValue = earliestDatapoint[1 /*The value at that datapoint */];
        returnData["open"] = openValue;
        returnData["current"] = currentValue;
        returnData["total"] = currentValue - openValue;
        returnData["percent"] = ((currentValue - openValue) / openValue) * 100;
        returnData["lastUpdated"] =
          currentDatapoint[0 /*The corresponding timestamp */];
      }
      return returnData;
    },
  },
  mutations: {
    /**
     * Adds timeseries data for a unique entity for a specific time granularity
     * @param {*} state
     * @param {*} params - id: The unique id of the entity the timeseries is related to
     *   - timeseriesData: An array of [timestamp, value] arrays
     *   - granularity: The time interval for which to update the data
     */
    [types.ADD_TIMESERIES_DATA](state, { id, timeseriesData, granularity }) {
      const timeseriesDataMap = Object.fromEntries(timeseriesData);
      let timeseriesToUpdate = {};
      if (Object.prototype.hasOwnProperty.call(state, id)) {
        if (Object.prototype.hasOwnProperty.call(state[id], granularity)) {
          // If the granularity has already been encountered, get the existing timeseries
          timeseriesToUpdate = state[id][granularity];
        } else {
          // Set the granularity object to be reactive
          Vue.set(state[id], granularity, {});
        }
      } else {
        // Set up the id object property to be reactive
        Vue.set(state, id, {});
        // Set the granularity object to be reactive
        Vue.set(state[id], granularity, {});
      }
      const mergedTimeseries = Object.assign(
        {},
        timeseriesToUpdate,
        timeseriesDataMap
      );
      state[id][granularity] = Object.assign({}, mergedTimeseries);
    },
  },
};
