import InvitesService from "@/services/invites.service.js";
import { INVITES as types } from "@/store/mutation-types.js";
import { createLookupFromListByKey } from "@/store/create-lookup-from-list-by-key.js";

export const platformInvites = {
  namespaced: true,
  state: {
    invites: {},
  },
  getters: {
    pendingInvites(state) {
      return Object.fromEntries(
        Object.entries(state.invites).filter(
          ([, invite]) => invite.status == "P"
        )
      );
    },
  },
  mutations: {
    [types.LOAD_INVITES](state, invites) {
      state.invites = createLookupFromListByKey(state.invites, invites, "id");
    },
    [types.UPDATE_INVITE](state, invite) {
      state.invites = createLookupFromListByKey(state.invites, [invite], "id");
    },
    [types.CREATE_INVITE](state, invite) {
      state.invites = createLookupFromListByKey(state.invites, [invite], "id");
    },
  },
  actions: {
    async initialize({ commit }) {
      const response = await InvitesService.getInvitesFromUser();
      commit(types.LOAD_INVITES, response.data);
    },
    async delete__invite({ commit }, id) {
      const response = await InvitesService.deletePlatformInvite(id);
      commit(types.UPDATE_INVITE, response.data);
    },
    async create__invite({ commit }, recipientEmail) {
      const response = await InvitesService.createPlatformInvite(
        recipientEmail
      );
      commit(types.CREATE_INVITE, response.data);
    },
  },
};
