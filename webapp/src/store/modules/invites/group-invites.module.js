import InvitesService from "@/services/invites.service.js";
import { INVITES as types } from "@/store/mutation-types.js";
import { createLookupFromListByKey } from "@/store/create-lookup-from-list-by-key.js";

export const groupInvites = {
  namespaced: true,
  state: {
    invites: {},
  },
  mutations: {
    [types.LOAD_INVITES](state, { invites, groupId }) {
      state.invites[groupId] = createLookupFromListByKey(
        state.invites[groupId],
        invites,
        "id"
      );
    },
    [types.UPDATE_INVITE](state, invite) {
      state.invites[invite.group] = createLookupFromListByKey(
        state.invites[invite.group],
        [invite],
        "id"
      );
    },
    [types.CREATE_INVITE](state, invite) {
      state.invites[invite.group] = createLookupFromListByKey(
        state.invites[invite.group],
        [invite],
        "id"
      );
    },
  },
  actions: {
    async load__invites({ commit }, groupId) {
      const response = await InvitesService.getInvitesToGroup(groupId);
      const invites = response.data;
      commit(types.LOAD_INVITES, { invites, groupId });
    },
    async delete__invite({ commit }, id) {
      const response = await InvitesService.deleteGroupInvite(id);
      commit(types.UPDATE_INVITE, response.data);
    },
    async create__invite({ commit }, { recipientEmail, groupId }) {
      const response = await InvitesService.createGroupInvite(
        recipientEmail,
        groupId
      );
      commit(types.CREATE_INVITE, response.data);
    },
  },
};
