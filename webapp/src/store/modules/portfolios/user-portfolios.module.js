import PortfoliosService from "@/services/portfolios.service.js";
import { PORTFOLIOS as types } from "@/store/mutation-types.js";
import { createLookupFromListByKey } from "@/store/create-lookup-from-list-by-key.js";
import { GRANULARITIES } from "@/constants/twelvedata-constants.js";

export const userPortfolios = {
  namespaced: true,
  state: {
    // Prefer flattening to an object keyed by unique identifier over list for O[1] lookup
    portfolios: {},
  },
  getters: {
    // Getters for portfolio properties
    portfolio_investments__fromPortfolioId: (state) => (portfolioId) => {
      return state.portfolios[portfolioId].investments;
    },
    portfolio_editableInfo__fromPortfolioId: (state) => (portfolioId) => {
      return {
        name: state.portfolios[portfolioId].name,
        description: state.portfolios[portfolioId].description,
      };
    },
    all_portfolioIds(state) {
      return Object.keys(state.portfolios);
    },
    // A mapping of all holdings across all portfolios and their value
    aggregateHoldings(state) {
      const holdings = {
        // ticker_symbol: value
      };
      const cashInvestmentTickerPrefix = "CUR:";
      for (const [, portfolio] of Object.entries(state.portfolios)) {
        portfolio.investments
          .filter(
            (investment) =>
              !investment.ticker_symbol.includes(cashInvestmentTickerPrefix)
          )
          .forEach((investment) => {
            let previousValue = 0;
            if (
              Object.prototype.hasOwnProperty.call(
                holdings,
                investment.ticker_symbol
              )
            ) {
              previousValue = holdings[investment.ticker_symbol];
            }
            holdings[investment.ticker_symbol] =
              previousValue + parseFloat(investment.current_value);
          });
      }
      return holdings;
    },
    // The cumulative value of holdings in all accounts
    aggregatePortfolioBalance(state) {
      return Object.entries(state.portfolios).reduce(function(
        totalBalance,
        [, portfolio]
      ) {
        return totalBalance + portfolio.balance_current;
      },
      0);
    },
    aggregateCostBasis(state) {
      return Object.entries(state.portfolios).reduce(function(
        totalCostBasis,
        [, portfolio]
      ) {
        return totalCostBasis + portfolio.total_cost_basis;
      },
      0);
    },
  },
  mutations: {
    [types.LOAD_PORTFOLIOS](state, portfolios) {
      state.portfolios = createLookupFromListByKey(
        state.portfolios,
        portfolios,
        "id"
      );
    },
    [types.ADD_PORTFOLIO](state, portfolio) {
      state.portfolios = createLookupFromListByKey(
        state.portfolios,
        [portfolio], // Pass the portfolio in as a list of one
        "id"
      );
    },
    [types.REMOVE_PORTFOLIO](state, portfolioId) {
      delete state.portfolios[portfolioId];
      // Force the deletion to occur reactively
      state.portfolios = Object.assign({}, state.portfolios);
    },
    [types.UPDATE_PORTFOLIO](state, { id, name, description }) {
      state.portfolios[id] = Object.assign({}, state.portfolios[id], {
        name: name,
        description: description,
      });
    },
  },
  actions: {
    async link({ commit, dispatch }, params) {
      const response = await PortfoliosService.link(
        params.name,
        params.description
      );
      const portfolio = response;
      commit(types.ADD_PORTFOLIO, portfolio);
      // Load the risk metrics for the newly linked portfolio
      dispatch(
        "portfolioRiskMetrics/load_single_portfolio_metrics__by_id",
        portfolio.id,
        {
          root: true,
        }
      );
    },
    async delete({ commit }, portfolioId) {
      PortfoliosService.unlink(portfolioId);
      commit(types.REMOVE_PORTFOLIO, portfolioId);
    },
    async update({ commit }, { id, name, description, active }) {
      const response = await PortfoliosService.patch(id, {
        name,
        description,
        active,
      });
      commit(types.UPDATE_PORTFOLIO, {
        id: id,
        name: response.data.name,
        description: response.data.description,
        active: response.data.active,
      });
    },
    /**
     * Triggers the api calls to load data about a users' portfolios. Bubble up
     * any potentially unresolved promises so that the top level initialize function
     * knows when the initialization process is complete.
     * @param {*} context
     * @returns {[Promise]} - An array of promises triggered by this function.
     */
    async initialize({ commit, dispatch, state }) {
      const response = await PortfoliosService.getUserPortfolios();
      commit(types.LOAD_PORTFOLIOS, response.data);
      const promises = [];
      // Load the most up to date stock information once the portfolios have been loaded
      Object.keys(state.portfolios).forEach((portfolioId) =>
        promises.push(
          dispatch(
            "portfolioHistory/refresh_portfolio_history",
            {
              portfolioId,
              granularity: GRANULARITIES.FIVE_MIN,
            },
            { root: true }
          )
        )
      );
      // Load related metrics once the portfolios have been loaded
      promises.push(
        dispatch("portfolioRiskMetrics/initialize", null, { root: true })
      );
      return Promise.all(promises);
    },
  },
};
