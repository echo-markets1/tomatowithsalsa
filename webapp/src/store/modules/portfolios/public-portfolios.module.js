import { PORTFOLIOS as types } from "@/store/mutation-types.js";
import { createLookupFromListByKey } from "@/store/create-lookup-from-list-by-key.js";

export const publicPortfolios = {
  namespaced: true,
  state: {
    // Prefer flattening to an object keyed by unique identifier over list for O[1] lookup
    portfolios: {},
  },
  getters: {
    // Getters for portfolio properties
    portfolio_investments__fromPortfolioId: (state) => (portfolioId) => {
      return state.portfolios[portfolioId].investments;
    },
  },
  mutations: {
    [types.LOAD_PORTFOLIOS](state, portfolios) {
      state.portfolios = createLookupFromListByKey(
        state.portfolios,
        portfolios,
        "id"
      );
    },
    [types.ADD_PORTFOLIO](state, portfolio) {
      state.portfolios = createLookupFromListByKey(
        state.portfolios,
        [portfolio], // Pass the portfolio in as a list of one
        "id"
      );
    },
    [types.REMOVE_PORTFOLIO](state, portfolioId) {
      delete state.portfolios[portfolioId];
      // Force the deletion to occur reactively
      state.portfolios = Object.assign({}, state.portfolios);
    },
  },
  actions: {},
};
