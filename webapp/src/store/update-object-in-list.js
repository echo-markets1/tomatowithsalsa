export function updateObjectInList(updatedObj, objList) {
  const updatedObjIndex = objList.findIndex((obj) => obj.id == updatedObj.id);
  objList.splice(updatedObjIndex, 1, updatedObj);
}
