// Takes an Object and an existing key, and nests the remaining fields of the
// under the provided key. The key should be able to uniquely identify the object
function _nestObjectUnderKey(obj, key) {
  const objLookupKey = obj[key];
  delete obj[key];
  return { [objLookupKey]: obj };
}

// Takes an existing map, a list of objects, and a key and places the objects in
// the lookup using the value returned by the key field as their look up value,
// merging the newLookup with the existing.
export function createLookupFromListByKey(existingLookup, objectList, key) {
  return objectList.reduce(function(newLookup, obj) {
    return Object.assign(
      newLookup,
      existingLookup,
      _nestObjectUnderKey(obj, key)
    );
  }, {});
}
