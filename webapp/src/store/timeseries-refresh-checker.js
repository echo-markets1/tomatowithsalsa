import {
  isTradingDay,
  hasMarketOpened,
} from "@/store/market-schedule-checker.js";
import { GRANULARITIES } from "@/constants/twelvedata-constants.js";

/**
 * Takes the timestamp of the last timeseries datapoints in ms, and the current
 * time as a JS Date, and evaluates whether or not new data is available for the
 * given granularity. For intraday data (< 1day), also validates that the market
 * has opened before trying to pull new data.
 * @param {String} granularity - The distance between datapoints on the timeseries
 * @param {Number} lastTimestamp - The most recent timestamp of the timeseries,
 *  provided in systemTime
 * @param {Date} currentTime
 * @returns {Boolean} - Whether or the not the timeseries data needs refreshing
 */
export function shouldRefreshTimeseries(
  granularity,
  lastTimestamp,
  currentTime
) {
  /*
    If lastTimestamp is 0, this means this is the first time the time series data
    is being loaded, and we should always refresh.
  */
  if (lastTimestamp == 0) {
    return true;
  }
  /*
    If this isn't the first refresh, we should not load new data on non trading
    days or premarket opening
  */
  if (!isTradingDay(currentTime) || !hasMarketOpened(currentTime)) {
    return false;
  }
  const oneMinInMS = 60000;
  // Granularities are based on https://twelvedata.com/docs#stocks
  switch (granularity) {
    case GRANULARITIES.FIVE_MIN: {
      const fiveMinsInMS = oneMinInMS * 5;
      return Math.abs(currentTime.getTime() - lastTimestamp) > fiveMinsInMS;
    }
    case GRANULARITIES.ONE_HOUR: {
      const oneHourInMS = oneMinInMS * 60;
      return Math.abs(currentTime.getTime() - lastTimestamp) > oneHourInMS;
    }
    case GRANULARITIES.ONE_DAY: {
      const oneDayInMS = oneMinInMS * 60 * 24;
      return Math.abs(currentTime.getTime() - lastTimestamp) > oneDayInMS;
    }
    case GRANULARITIES.ONE_WEEK: {
      const oneWeekInMS = oneMinInMS * 60 * 24 * 7;
      return Math.abs(currentTime.getTime() - lastTimestamp) > oneWeekInMS;
    }
    default:
      throw `Timeseries data for the granularity of '${granularity}' is not supported.`;
  }
}
