/**
 * A simple heuristic to estimate whether or not the market has opened for a given day
 * @param {Date} currentTime
 * @returns {Boolean} - Whether or not currentTime is after market opening
 */
export function hasMarketOpened(currentTimeDate) {
  const marketOpen = new Date(currentTimeDate.getTime());
  marketOpen.setUTCHours(14, 30);
  return currentTimeDate.getTime() > marketOpen.getTime();
}

/**
 * A simple heuristic to estimate whether or not a given date was a trading day
 * TODO: [https://app.clickup.com/t/1adypcp] Needs to be updated to account for
 * banking holidays.
 * @param {Date} date
 * @returns {Boolean} - Whether or not the given date falls on a day the market is open
 */
export function isTradingDay(date) {
  const weekendDays = [0 /*Sun*/, 6 /*Sat*/];
  return !weekendDays.includes(date.getUTCDay());
}

/**
 *
 * @returns {Date} - Date object representing the most recent trading day
 */
export function getMostRecentTradingDay() {
  const now = new Date();
  const oneDayMS = 86400000;
  // Set the date to be return as today by default
  let lastTradingDay = new Date(now.getTime());
  /*
   * If the market is not open now or has not opened yet, set the Date to be
   * returned as yesterday
   */
  if (!hasMarketOpened(now)) {
    lastTradingDay = new Date(now.getTime() - oneDayMS);
  }
  /*
   * If the current date to be returned is not a trading day, check the previous
   * day, and repeat until a trading day is found
   */
  while (!isTradingDay(lastTradingDay)) {
    lastTradingDay = new Date(lastTradingDay.getTime() - oneDayMS);
  }
  return lastTradingDay;
}
