import Vue from "vue";
import Vuex from "vuex";

import { auth } from "./modules/auth.module";
import { accounts } from "./modules/accounts.module";
import { platformInvites } from "./modules/invites/platform-invites.module";
import { portfolios } from "./modules/portfolios.module";
import { transactions } from "./modules/transactions.module";
import { portfolioHistory } from "./modules/historical-data/portfolio-history.module";
import { compositeHistory } from "./modules/historical-data/composite-history.module";
import { portfolioRiskMetrics } from "./modules/risk-metrics/portfolio-risk-metrics.module";
import { portfolioSuperlativePerformanceMetrics } from "./modules/historical-data/portfolio-superlative-performance-metrics-module";
import { stockHistory } from "./modules/historical-data/stock-history.module";
import { stockQuotes } from "./modules/stock-quotes.module";
import { unsupportedStocks } from "./modules/unsupported-stocks.module";
import { groups } from "./modules/groups.module";
import { groupInvites } from "./modules/invites/group-invites.module";
import { groupPosts } from "./modules/posts/group-posts.module";
import { memberships } from "./modules/memberships.module";
import { global } from "./modules/global.module";
import { publicPosts } from "./modules/posts/public-posts.module.js";

Vue.use(Vuex);
const NAMESPACED_MODULES_TO_INITIALIZE = [
  "accounts",
  "portfolios",
  "groups",
  "platformInvites",
];

export default new Vuex.Store({
  modules: {
    auth,
    accounts,
    portfolios,
    transactions,
    platformInvites,
    portfolioHistory,
    compositeHistory,
    portfolioRiskMetrics,
    portfolioSuperlativePerformanceMetrics,
    stockHistory,
    stockQuotes,
    unsupportedStocks,
    groups,
    groupInvites,
    groupPosts,
    memberships,
    global,
    publicPosts,
  },
  actions: {
    async initialize({ dispatch }) {
      const promises = [];
      NAMESPACED_MODULES_TO_INITIALIZE.forEach((module) =>
        promises.push(dispatch(`${module}/initialize`))
      );
      await Promise.all(promises);
    },
  },
  strict: process.env.NODE_ENV !== "production",
});
