// A time series chart optimized for displaying composite timeseries performance.
// Includes a toggle for the data timeline of the time series graph. Displays
// information about the Portfolio's performance as a whole as well.
<template>
  <div class="px-2">
    <div class="d-flex flex-row justify-content-between">
      <div class="ml-1">
        <template v-if="showInvestedValues">
          <h2>
            <strong>
              {{
                parseFloat(aggregatePortfolioBalance) | currencyFormat
              }}</strong
            >
          </h2>
          <h4>
            <CashChangeOverTimeSpan :cashChangeValue="cashReturn" />
            <PercentChangeOverTimeSpan :percentChangeValue="percentReturn" />
          </h4>
        </template>
        <template v-else>
          <h2>
            <PercentChangeOverTimeSpan :percentChangeValue="percentReturn" />
          </h2>
        </template>
      </div>
      <div>
        <template v-if="compare">
          <StocksTabs :tabs="benchmarks" />
        </template>
      </div>
      <div>
        <TimeSeriesSelectorButtons
          :timelines="timelines"
          :selectedTimeline="selectedTimeline"
        />
      </div>
    </div>
    <b-skeleton-wrapper :loading="loading">
      <template #loading>
        <b-skeleton-img no-aspect width="100%" :height="toPxStr(height)" />
      </template>
      <BaseTimeSeriesChart
        :chartType="chartType"
        :isSparkline="isSparkline"
        :height="height"
        :series="series"
        :selectedTimeline="selectedTimeline"
        :showDataTooltips="showInvestedValues"
      />
    </b-skeleton-wrapper>
  </div>
</template>

<script>
import { GRANULARITIES } from "@/constants/twelvedata-constants.js";
import numberToPixelStrMixin from "@/mixins/number-to-pixel-str-mixin.js";
import BaseTimeSeriesChart from "@/components/base/charts/apex/BaseTimeSeriesChart.vue";
import TimeSeriesSelectorButtons from "@/components/base/TimeSeriesSelectorButtons.vue";
import CashChangeOverTimeSpan from "@/components/base/CashChangeOverTimeSpan.vue";
import PercentChangeOverTimeSpan from "@/components/base/PercentChangeOverTimeSpan.vue";
import StocksTabs from "@/components/stock/StocksTabs.vue";

export default {
  name: "CompositeTimeSeriesChart",
  mixins: [numberToPixelStrMixin],
  components: {
    BaseTimeSeriesChart,
    TimeSeriesSelectorButtons,
    CashChangeOverTimeSpan,
    PercentChangeOverTimeSpan,
    StocksTabs,
  },
  props: {
    userId: {
      type: String,
      required: true,
    },
    chartType: {
      // This component is optimized for use as ( "line" | "area")
      type: String,
      required: true,
    },
    isSparkline: {
      type: Boolean,
      default: false,
    },
    showInvestedValues: {
      // Several use cases require us to not reveal the raw value of cash invested
      type: Boolean,
      default: true,
    },
    height: {
      type: Number,
      default: 300,
    },
    compare: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      selectedTimeline: "ALL",
      timeseriesData: [[]],
      stockTimeseriesData: [[]],
      processing: false,
      currentBenchmark: "S&P",
    };
  },
  computed: {
    loading() {
      return this.userId == null || this.processing;
    },
    aggregatePortfolioBalance() {
      return this.$store.getters[
        "portfolios/userPortfolios/aggregatePortfolioBalance"
      ];
    },
    cashReturn() {
      return (
        this.aggregatePortfolioBalance -
        this.$store.getters["portfolios/userPortfolios/aggregateCostBasis"]
      );
    },
    percentReturn() {
      return (
        ((this.aggregatePortfolioBalance -
          this.$store.getters["portfolios/userPortfolios/aggregateCostBasis"]) /
          this.$store.getters["portfolios/userPortfolios/aggregateCostBasis"]) *
        100
      );
    },
    selectedGranularity() {
      // Granularities are based on https://twelvedata.com/docs#stocks
      switch (this.selectedTimeline) {
        case "1D":
          return GRANULARITIES.FIVE_MIN;
        case "1W":
          return GRANULARITIES.ONE_HOUR;
        case "1M":
        case "3M":
          return GRANULARITIES.ONE_DAY;
        case "1Y":
        case "ALL":
          return GRANULARITIES.ONE_WEEK;
        default:
          throw `'${this.selectedTimeline}' is not supported by CompositeTimeSeriesChart`;
      }
    },
    series() {
      /*
       * Formatting for apex charts needs to look like this:
       * series: [
       *   {
       *     name: "CompositeTimeseries",
       *     data: [
       *       [1327359600000, 30.95],
       *       [1327446000000, 31.34]
       *     ],
       *   },
       * ],
       */
      return [
        {
          name: "Composite Perfomance",
          data: this.timeseriesData,
        },
        {
          name: "Benchmark Perfomance",
          data: this.stockTimeseriesData,
        },
      ];
    },
  },
  methods: {
    async setTimeseriesData() {
      if (this.userId != null) {
        this.processing = true;
        await this.$store.dispatch(
          "compositeHistory/refresh_composite_history",
          {
            userId: this.userId,
            granularity: this.selectedGranularity,
          }
        );
        this.timeseriesData = this.$store.getters[
          "compositeHistory/reverse_chronological_timeseries__from_id_granularity"
        ]({
          id: this.userId,
          granularity: this.selectedGranularity,
        });

        if (this.compare) {
          await this.$store.dispatch(
            "stockHistory/refresh_single_symbol_data",
            {
              symbol: this.currentStock,
              granularity: this.selectedGranularity,
            }
          );
          this.stockTimeseriesData = this.$store.getters[
            "stockHistory/reverse_chronological_timeseries__from_id_granularity"
          ]({
            id: this.currentStock,
            granularity: this.selectedGranularity,
          });
        }
        this.processing = false;
      }
    },
    async setBenchmarkTimeseriesData() {
      if (this.userId != null && this.compare) {
        this.processing = true;
        await this.$store.dispatch("stockHistory/refresh_single_symbol_data", {
          symbol: this.currentBenchmark,
          granularity: this.selectedGranularity,
        });
        this.stockTimeseriesData = this.$store.getters[
          "stockHistory/reverse_chronological_timeseries__from_id_granularity"
        ]({
          id: this.currentBenchmark,
          granularity: this.selectedGranularity,
        });
        this.processing = false;
      }
    },
  },
  watch: {
    userId: {
      handler: "setTimeseriesData",
      immediate: true,
    },
  },
  created() {
    this.timelines = ["1D", "1W", "1M", "3M", "1Y", "ALL"];
    this.benchmarks = ["S&P", "DJIA", "COMP"];
    /*
     * Initially, only listen to time selection events from child components.
     * We may opt to reorganize this event structure to be more universal later on,
     * but for now components should only respond to time-selected events from a child
     * for a consistent and simple UX
     */
    this.$on("timeline-selected", (timeline) => {
      this.selectedTimeline = timeline;

      this.setTimeseriesData();
    });

    this.$on("stocks-tab-selected", (tabName) => {
      this.currentBenchmark = tabName;
      this.setStockTimeseriesData();
    });
  },
};
</script>
