// Account Info/Settings Keys/Constants
const HANDLE = "Handle";
const FIRSTNAME = "First name";
const LASTNAME = "Last name";
const DOB = "Date of Birth";

export default {
  HANDLE: HANDLE,
  FIRSTNAME: FIRSTNAME,
  LASTNAME: LASTNAME,
  DOB: DOB,
};
