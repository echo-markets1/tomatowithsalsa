// Maps the BE name of the Account Info/Settings Keys to the display name
import accountInfoDisplayNames from "@/components/account/modals/account-info-display-names.js";

// Must be maintained in the order we wish to display
export default {
  // key: value = backend_name: displayName
  handle: accountInfoDisplayNames.HANDLE,
  first_name: accountInfoDisplayNames.FIRSTNAME,
  last_name: accountInfoDisplayNames.LASTNAME,
  date_of_birth: accountInfoDisplayNames.DOB,
};
