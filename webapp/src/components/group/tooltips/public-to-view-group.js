export default {
  title: "Publicly Visible Groups",
  description:
    "The more the merrier! Anyone can view a publicly visible group. Participation in this group still requires membership, however.",
};
