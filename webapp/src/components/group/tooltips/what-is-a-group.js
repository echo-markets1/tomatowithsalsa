export default {
  title: "What is a group?",
  description:
    "A group is place for sharing and comparing your portfolios, and collaborating investment strategies",
};
