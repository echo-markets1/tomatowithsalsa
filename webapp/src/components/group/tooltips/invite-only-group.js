export default {
  title: "Invite Only Groups",
  description:
    "Keep the chaos to a minimum: This group can not be found on searches. Group members must be invited, and only group members can see whats going on.",
};
