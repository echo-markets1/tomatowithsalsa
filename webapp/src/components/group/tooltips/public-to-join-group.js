export default {
  title: "Public Groups",
  description: "The more the merrier! Anyone can view and join a public group.",
};
