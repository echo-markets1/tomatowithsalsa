export default {
  title: "Private Groups",
  description:
    "Keep the chaos to a minimum: Group members must request to join or be invited, and only group members can see whats going on",
};
