import { initGlobalOptions } from "@/components/base/charts/creative_tim/config";
import "./roundedCornersExtension";
export default {
  mounted() {
    initGlobalOptions();
  },
};
