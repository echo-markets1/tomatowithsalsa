import {PluginKey} from "prosemirror-state";
import {VueRenderer} from "@tiptap/vue-2";
import MentionList from "@/components/editor/MentionList";
import tippy from "tippy.js";
import SearchService from "@/services/search/search.service";

const suggestion = (sign, array, key) => {
  return {
    char: sign,
    pluginKey: new PluginKey(key),
    items: ({query}) => {
      return array
        .filter((item) => item.toLowerCase().startsWith(query.toLowerCase()))
        .slice(0, 5);
    },
    render: renderObject
  }
}

const stockSuggestion = (sign, key) => {
  return {
    char: sign,
    pluginKey: new PluginKey(key),
    items: async ({query}) => {
      let stocks = [];
      const response = await SearchService.searchStocks(query)
      response.data.data.forEach(item => stocks.push(item.symbol));
      return stocks.filter((item) => item.toLowerCase().startsWith(query.toLowerCase()))
        .slice(0, 5)
    },
    render: renderObject
  }
}

const renderObject = () => {
  let component
  let popup

  return {
    onStart: props => {
      component = new VueRenderer(MentionList, {
        parent: this,
        propsData: props,
      })

      popup = tippy('body', {
        getReferenceClientRect: props.clientRect,
        appendTo: () => document.body,
        content: component.element,
        showOnCreate: true,
        interactive: true,
        trigger: 'manual',
        placement: 'bottom-start',
      })
    },

    onUpdate(props) {
      component.updateProps(props)

      popup[0].setProps({
        getReferenceClientRect: props.clientRect,
      })
    },

    onKeyDown(props) {
      if (props.event.key === 'Escape') {
        popup[0].hide()

        return true
      }

      return component.ref?.onKeyDown(props)
    },

    onExit() {
      popup[0].destroy()
      component.destroy()
    },
  }
}

export {suggestion, stockSuggestion};