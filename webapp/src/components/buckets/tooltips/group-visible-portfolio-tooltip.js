export default {
  title: "Group Visible Portfolio",
  description: "This portfolio is visible to other members of common groups.",
};
