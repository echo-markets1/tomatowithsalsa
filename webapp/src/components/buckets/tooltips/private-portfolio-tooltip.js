export default {
  title: "Private Portfolio",
  description:
    "This portfolio is not shared with any groups, and is visible only to you.",
};
