// core Vue imports
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// Plug in global filters
import "./filters";

// A plugin file where you could register global directives
import GlobalDirectives from "@/plugins/globalDirectives";
import GlobalComponents from "@/plugins/globalComponents";

import "@/assets/sass/app.scss";

// Axios for http requests
import axios from "axios";
// Make it global
Vue.prototype.axios = axios;
axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";

import Trend from "vuetrend";
Vue.use(Trend);

// Install BootstrapVue
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

Vue.use(BootstrapVue);

// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
// Vue Modal plugin
import VModal from "vue-js-modal";
Vue.use(VModal);

// Vue gridlayout plugin
import VueGridLayout from "vue-grid-layout";
Vue.use(VueGridLayout);

// Client-side validation of in forms
import VeeValidate from "vee-validate";
Vue.use(VeeValidate, {
  fieldsBagName: "veeFields",
  errorBagName: "veeErrors"
  // See https://github.com/bootstrap-vue/bootstrap-vue/issues/1270 for more info
});
Vue.use(GlobalDirectives);
Vue.use(GlobalComponents);

// slide up down for global search
import SlideUpDown from "vue-slide-up-down";
Vue.component("slide-up-down", SlideUpDown);

import Popper from "vue-popperjs";
Vue.component("popper", Popper);

import truncate from "vue-truncate-collapsed";
Vue.component("truncate", truncate);

import { Select, Option } from "element-ui";
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
locale.use(lang)
Vue.use(Select);
Vue.use(Option);

import infiniteScroll from "vue-infinite-scroll";
Vue.use(infiniteScroll);

// Icon library components from font awesome
import { library } from "@fortawesome/fontawesome-svg-core";
import {
  FontAwesomeIcon,
  FontAwesomeLayers,
  FontAwesomeLayersText,
} from "@fortawesome/vue-fontawesome";
// Import specific icons
import {
  faUserAstronaut,
  faBell,
  faCog,
  faInfoCircle,
  faChartLine,
  faUsers,
  faEye,
  faRocket,
  faMoneyBillWave,
  faPiggyBank,
  faCheckCircle,
  faDonate,
  faCashRegister,
  faLandmark,
  faPercentage,
  faSnowflake,
  faFireAlt,
  faTheaterMasks,
  faGlobeAmericas,
  faReceipt,
  faChartPie,
  faCalendarAlt,
  faChartArea,
  faMinusCircle,
  faMoneyCheckAlt,
  faListAlt,
  faShoppingBasket,
  faPlusSquare,
  faPlusCircle,
  faHome,
  faBinoculars,
  faTrophy,
  faEllipsisH,
  faEllipsisV,
  faBalanceScaleRight,
  faEdit,
  faTrashAlt,
  faLock,
  faUserCog,
  faClone,
  faCaretDown,
  faCaretUp,
  faThumbtack,
  faPencilAlt,
  faClock,
  faComments,
  faLockOpen,
  faEnvelope,
  faAngleRight,
  faAngleLeft,
  faHeart,
  faThumbsUp,
  faBold,
  faItalic,
  faListUl,
  faFileImage,
  faLink,
  faExclamationTriangle,
  faEyeSlash,
  faUser,
  faCogs,
  faSignOutAlt
} from "@fortawesome/free-solid-svg-icons";
// Then register them
library.add(
  faUserAstronaut,
  faBell,
  faCog,
  faInfoCircle,
  faChartLine,
  faUsers,
  faEye,
  faRocket,
  faMoneyBillWave,
  faPiggyBank,
  faCheckCircle,
  faDonate,
  faCashRegister,
  faLandmark,
  faPercentage,
  faSnowflake,
  faFireAlt,
  faTheaterMasks,
  faGlobeAmericas,
  faReceipt,
  faChartPie,
  faCalendarAlt,
  faChartArea,
  faMinusCircle,
  faMoneyCheckAlt,
  faListAlt,
  faShoppingBasket,
  faPlusSquare,
  faPlusCircle,
  faHome,
  faBinoculars,
  faTrophy,
  faEllipsisH,
  faEllipsisV,
  faBalanceScaleRight,
  faEdit,
  faTrashAlt,
  faLock,
  faUserCog,
  faClone,
  faCaretDown,
  faCaretUp,
  faThumbtack,
  faPencilAlt,
  faClock,
  faComments,
  faLockOpen,
  faEnvelope,
  faAngleRight,
  faAngleLeft,
  faHeart,
  faThumbsUp,
  faBold,
  faItalic,
  faListUl,
  faFileImage,
  faLink,
  faAngleLeft,
  faExclamationTriangle,
  faEyeSlash,
  faUser,
  faCogs,
  faSignOutAlt
);
Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component("font-awesome-layers", FontAwesomeLayers);
Vue.component("font-awesome-layers-text", FontAwesomeLayersText);

// Register Modals for global use
// Shortcut for accessing the App.vue instance from other components
Vue.prototype.$app = function() {
  return this.$root.$children.find((child) => child.$options.name === "App");
};
Vue.config.productionTip = false;

// Create an eventBus for inter-component communication
export const eventBus = new Vue();

// import VueNativeSock from "vue-native-websocket";
// Vue.use(
//   VueNativeSock,
//   "wss://ws.twelvedata.com/v1/quotes/price?apikey=1f40fd119c8e4d39be953bd70caf6f5c",
//   { store: store.stocks }
// );

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
