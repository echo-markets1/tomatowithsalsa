export default {
  methods: {
    validateLastName(lastName) {
      // TODO: Validate uniqueness of username
      if (lastName.value.length > 30) {
        lastName.state = false;
        lastName.error =
          "This is a beautiful name, but too long for us to store at this time.";
      } else if (lastName.value.length == 0) {
        lastName.state = false;
        lastName.error = "Whoops, you forgot to tell us your last name!";
      } else {
        lastName.error = null;
        lastName.state = true;
      }
    },
  },
};
