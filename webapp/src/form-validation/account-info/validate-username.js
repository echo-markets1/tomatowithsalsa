export default {
  methods: {
    validateHandle(username) {
      // TODO: [https://app.clickup.com/t/v9db5z] Validate uniqueness of username
      if (username.value.length > 75) {
        username.state = false;
        username.error = "Good thinking, but that username is too long!";
      } else if (username.value.length == 0) {
        username.state = false;
        username.error =
          "Whoops, how will other users recognize you without a username?";
      } else {
        username.error = null;
        username.state = true;
      }
    },
  },
};
