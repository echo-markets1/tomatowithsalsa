export default {
  methods: {
    validateDateOfBirth(dob) {
      // www.geeksforgeeks.org/how-to-check-a-date-is-valid-or-not-using-javascript/
      Date.prototype.isValid = function() {
        // If the date object is invalid it
        // will return 'NaN' on getTime()
        // and NaN is never equal to itself.
        return this.getTime() === this.getTime();
      };
      const dobAsDate = new Date(dob.value);
      if (!dobAsDate.isValid()) {
        dob.error = `${dob.value} is not a valid Date`;
        dob.state = false;
      } else {
        const today = new Date();
        const thirteenYears = new Date(today);
        thirteenYears.setFullYear(today.getFullYear() - 13);
        if (dobAsDate >= thirteenYears) {
          dob.error = "Must be 13 years of age or older to join";
          dob.state = false;
        } else {
          dob.error = null;
          dob.state = true;
        }
      }
    },
  },
};
