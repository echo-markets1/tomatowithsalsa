export default {
  methods: {
    validateFirstName(firstName) {
      // TODO: Validate uniqueness of username
      if (firstName.value.length > 30) {
        firstName.state = false;
        firstName.error =
          "This is a beautiful name, but too long for us to store at this time.";
      } else if (firstName.value.length == 0) {
        firstName.state = false;
        firstName.error = "Whoops, you forgot to tell us your first name!";
      } else {
        firstName.error = null;
        firstName.state = true;
      }
    },
  },
};
