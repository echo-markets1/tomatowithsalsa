export default {
  methods: {
    validateEmail(email) {
      // Regex from: https://stackoverflow.com/a/46181
      const validEmailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!validEmailRegex.test(email.value)) {
        email.state = false;
        email.error = "Good try, thats not a real email";
      } else if (email.value.length > 253) {
        email.state = false;
        email.error =
          "Whoa, this is too much email for us to handle! Do you have a shorter one?";
      } else {
        email.error = null;
        email.state = true;
      }
    },
  },
};
