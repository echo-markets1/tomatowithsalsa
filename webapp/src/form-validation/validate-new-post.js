import { Validator } from "vee-validate";

export default {
  methods: {
    validateNewPost(post) {
      if (post.value.length > 1000) {
        post.state = false;
        post.error =
          "Shoot, it looks like we ran out of room. Try breaking this up into multiple posts.";
      } else if (post.value.length == 0) {
        post.state = false;
        post.error =
          "Whoops, looks like all those great thoughts are still bottled up in your brain!";
      } else {
        post.error = null;
        post.state = true;
      }
    },
    setVeePostValidator({
      maxLength = 3000,
      minLength = 7 /* Defaults to <p></p>, 7 characters */,
    }) {
      Validator.extend("max-post-length", {
        getMessage: () =>
          "Shoot, it looks like we ran out of room. Try breaking this up into multiple posts.",
        validate: (value) => value.length < maxLength,
      });
      Validator.extend("min-post-length", {
        getMessage: () =>
          "Whoops, looks like all those great thoughts are still bottled up in your brain!",
        validate: (value) => value.length > minLength,
      });
    },
  },
};
