import Vue from "vue";

/*
 * Regex converting a number to the format of commas every three numbers from right
 * of the decimal point (Standard en-US format)
 */
var NUMBER_FORMAT_REGEX = new RegExp(/(\d)(?=(\d{3})+(?!\d))/, "g");

// Global Filters

/* 
Filters database datetimes into a more user friendly format by
conversion to a javascript Date object.  Uses the default Date
print format for the locality of the browser (ie. in en-US 
MM/DD/YYYY H:MM:SS AM/PM).

Different actions could be leveraged in the future due to javascript 
support for alternate print formats.
*/
Vue.filter("humanizeDateTime", (datetime) => {
  const date = new Date(datetime);
  const delta = Math.round((new Date() - date) / 1000);
  const minute = 60,
    hour = minute * 60,
    day = hour * 24;

  if (delta < day * 2) {
    let fuzzy = null;

    if (delta < 30) {
      fuzzy = "Just now";
    } else if (delta < minute) {
      fuzzy = delta + " seconds ago";
    } else if (delta < 2 * minute) {
      fuzzy = "A minute ago";
    } else if (delta < hour) {
      fuzzy = Math.floor(delta / minute) + " minutes ago";
    } else if (Math.floor(delta / hour) == 1) {
      fuzzy = "1 hour ago.";
    } else if (delta < day) {
      fuzzy = Math.floor(delta / hour) + " hours ago";
    } else {
      fuzzy = "Yesterday at " + date.toLocaleTimeString();
    }
    return fuzzy;
  }
  return date.toLocaleString();
});

// Takes a Number value, adds commas where apropriate, rounded to 2 decimals
Vue.filter("numberFormat", (val) => {
  return `${parseFloat(val)
    .toFixed(2)
    .replace(NUMBER_FORMAT_REGEX, "$1,")}`;
});

/*
 * Takes a percent value, adds commas where apropriate, a trailing % sign, and
 * a leading + sign for positive numbers (negative numbers already have a - sign)
 */

Vue.filter("percentChangeFormat", (val) => {
  if (val >= 0.0) {
    return `+${parseFloat(val)
      .toFixed(2)
      .replace(NUMBER_FORMAT_REGEX, "$1,")}%`;
  } else {
    return `${parseFloat(val)
      .toFixed(2)
      .replace(NUMBER_FORMAT_REGEX, "$1,")}%`;
  }
});

// Takes a percent value, adds commas where apropriate,  and a trailing % sign
Vue.filter("percentFormat", (val) => {
  return `${parseFloat(Math.abs(val))
    .toFixed(2)
    .replace(NUMBER_FORMAT_REGEX, "$1,")}%`;
});

/**
 * Takes a raw number as input and filters into the following format of string
 * $1,234,567.89
 * ie. Dollar sign, two decimals, and commas every three numbers from right of the
 * decimal to left
 */
Vue.filter("currencyFormat", (val) => {
  return `$${parseFloat(val)
    .toFixed(2)
    .replace(NUMBER_FORMAT_REGEX, "$1,")}`;
});

/**
 * Takes a raw number as input and filters into the following format of string
 * (+/-)$1,234,567.89
 * ie. Dollar sign, two decimals, and commas every three numbers from right of the
 * decimal to left, and a leading + or - sign
 */
Vue.filter("currencyChangeFormat", (val) => {
  if (val >= 0.0) {
    return `+$${parseFloat(val)
      .toFixed(2)
      .replace(NUMBER_FORMAT_REGEX, "$1,")}`;
  } else {
    return `${parseFloat(val)
      .toFixed(2)
      .replace(NUMBER_FORMAT_REGEX, "$1,")
      .replace("-", "-$")}`;
  }
});

/**
 * Filter used for condensing numbers like followers, members, etc. from raw values
 * to condensed values.
 * Ex: 1000 -> 1.0k, 2357000 -> 2.36m
 */
Vue.filter("condensedNumberFormat", (num) => {
  const thousands = "K";
  const millions = "M";
  const billions = "B";
  const trillions = "T";
  const condense = (val, ratio) => {
    return parseFloat(val / ratio).toPrecision(3);
  };
  const append = (val, suffix) => {
    return `${val}${suffix}`;
  };
  const fuzz = (val, ratio, suffix) => {
    return append(condense(val, ratio), suffix);
  };
  if (num < 1000) {
    return `${num}`;
  } else if (num < 1000000) {
    return fuzz(num, 1000, thousands);
  } else if (num < 1000000000) {
    return fuzz(num, 1000000, millions);
  } else if (num < 1000000000000) {
    return fuzz(num, 1000000000, billions);
  } else {
    return fuzz(num, 1000000000000, trillions);
  }
});
