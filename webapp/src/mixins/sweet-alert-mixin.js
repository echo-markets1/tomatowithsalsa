// Mixin to simplify the generation of Sweet Alerts in components. See the
// docs: https://sweetalert2.github.io/ for more details and the creativetim
// dashboard for examples: https://demos.creative-tim.com/bootstrap-vue-argon-dashboard-pro/?_ga=2.23903610.193658572.1606840246-279538365.1606840246#/components/notifications
import swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.css";
export default {
  methods: {
    showSwal(
      type,
      title,
      messageText = null,
      messageHTML = null,
      buttonsStyling = false
    ) {
      const swalConfig = {
        titleText: title,
        text: messageText,
        html: messageHTML,
        buttonsStyling: buttonsStyling,
      };
      switch (type) {
        case "basic": {
          swal.fire({
            ...swalConfig,
            confirmButtonClass: "btn btn-primary",
          });
          break;
        }
        case "info": {
          swal.fire({
            ...swalConfig,
            confirmButtonClass: "btn btn-info",
            icon: "info",
          });
          break;
        }
        case "success": {
          swal.fire({
            ...swalConfig,
            confirmButtonClass: "btn btn-success",
            icon: "success",
          });
          break;
        }
        case "warning": {
          swal.fire({
            ...swalConfig,
            confirmButtonClass: "btn btn-warning",
            icon: "warning",
          });
          break;
        }
        case "question": {
          swal.fire({
            ...swalConfig,
            confirmButtonClass: "btn btn-default",
            icon: "question",
          });
          break;
        }
        default:
          throw `Sweet Alerts of type ${type} are not yet supported. Please the see the sweet-alert-mixin for more details.`;
      }
    },
  },
};
