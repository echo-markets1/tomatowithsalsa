export default {
  methods: {
    makeToastNotification(msg, title, delay = 10000) {
      this.$root.$bvToast.toast(msg, {
        title: title,
        autoHideDelay: delay,
        appendToast: true,
      });
    },
  },
};
