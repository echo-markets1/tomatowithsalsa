import PlaidLinkService from "@/services/plaid/plaid-link.service";
import sweetAlertMixin from "@/mixins/sweet-alert-mixin.js";
export default {
  mixins: [sweetAlertMixin],
  data() {
    return {
      activeTimer: false,
    };
  },
  methods: {
    handleTimeout(eventName, metadata) {
      if (this.activeTimer) {
        console.log(
          `Plaid timed out while handling the '${eventName}' event at ${metadata.timestamp}. request_id: ${metadata.request_id}. link_session_id: ${metadata.link_session_id}. See the plaid dashboard for more details. Forcing exit from link flow by destroying the handler instance.`
        );
        window.plaidHandler.destroy();
        this.showSwal(
          "warning",
          "Warning",
          null,
          "Plaid experienced issues trying to process your request. Please visit <a href='https://plaid.com/trouble-connecting/'>plaid's website</a> for more details and troubleshooting tips."
        );
      } else {
        console.log(`'${eventName}' succeeded before timer completed.`);
      }
    },
    async getPlaidHandler() {
      const createLinkTokenResponse = await PlaidLinkService.createLinkToken();
      return window.Plaid.create({
        token: createLinkTokenResponse.data.link_token,
        onSuccess: (public_token, metadata) => {
          this.activeTimer = false;
          PlaidLinkService.setAccessToken(public_token, metadata);
        },
        onLoad: () => {},
        onExit: (error, metadata) => {
          this.activeTimer = false;
          console.log(
            `The account link workflow was exitted during: ${metadata.view_name}`
          );
          if (error != null) {
            console.log(`The workflow was exitted because of an error in the process.
              error_code: ${error.error_code}
              message: ${error.display_message || error.error_message}
              request_id: ${metadata.request_id}. link_session_id: ${
              metadata.link_session_id
            }. See the plaid dashboard for more details.
            `);
          }
        },
        onEvent: (eventName, metadata) => {
          this.activeTimer = false;
          this.lastEventTimestamp = metadata.timestamp;
          if (eventName === "ERROR" && metadata.error_code != null) {
            console.log(
              `An error occurred while handling the '${eventName}' event. Error: '${metadata.error_message}'. request_id: ${metadata.request_id}. link_session_id: ${metadata.link_session_id}. See the plaid dashboard for more details.`
            );
          } else if (eventName === "SUBMIT_CREDENTIALS") {
            this.activeTimer = true;
            console.log(`Setting a timer for plaid event: '${eventName}'`);
            setTimeout(
              this.timeoutHandler,
              this.twentySecondsInMS,
              eventName,
              metadata
            );
          }
        },
        receivedRedirectUri: null,
      });
    },
    async openLinkPortfolioForm() {
      window.plaidHandler = await this.getPlaidHandler();
      window.plaidHandler.open();
    },
  },
  created() {
    this.timeoutHandler = function(eventName, metadata) {
      this.handleTimeout(eventName, metadata);
    }.bind(this); // Gives function access to the components context at execution time.
    this.twentySecondsInMS = 20000;
  },
};
