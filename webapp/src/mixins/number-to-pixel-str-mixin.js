export default {
  methods: {
    toPxStr(val) {
      // Helper function to convert a value to string representing pixels
      return `${val}px`;
    },
  },
};
