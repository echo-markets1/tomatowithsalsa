export default {
    methods: {
        removeTradingViewFooter() {
            let externalLinkElements = document.getElementsByClassName("tradingview-widget-copyright");
            for(let i = 0; i < externalLinkElements.length; i++) {
              let el = externalLinkElements[i];
              el.parentNode.removeChild(el);
            }
          }
    },
  };
  