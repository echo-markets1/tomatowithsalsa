export default {
  methods: {
    // Override the unimplemented methods in the component to handle different scroll events
    handleScrollToBottom() {},
    handleScrollToTop() {},
    handleScroll() {},

    // Method with hooks for handling specific scroll events
    onScroll({ target: { scrollTop, clientHeight, scrollHeight } }) {
      if (scrollTop + clientHeight >= scrollHeight) {
        this.handleScrollToBottom();
      } else if (scrollTop == 0) {
        this.handleScrollToTop();
      } else {
        this.handleScroll();
      }
    },
  },
};
