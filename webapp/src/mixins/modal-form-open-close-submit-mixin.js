export default {
  methods: {
    open() {
      this.load();
      this.$modal.show(this.modalName);
    },
    close() {
      this.reset();
      this.$modal.hide(this.modalName);
    },
    handleSubmit() {
      throw "The required function 'handleSubmit()' is unimplemented by the component";
    },
    load() {
      throw "The required function 'load()' is unimplemented by the component";
    },
    reset() {
      throw "The required function 'reset()' is unimplemented by the component";
    },
  },
  mounted() {
    if (this.modalName === undefined) {
      throw "The required data field 'modalName' has not been set by the component";
    }
  },
};
