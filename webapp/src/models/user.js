export default class User {
  constructor(
    email,
    first_name,
    last_name,
    // date_of_birth,
    password1,
    password2
  ) {
    this.email = email;
    this.first_name = first_name;
    this.last_name = last_name;
    // this.date_of_birth = date_of_birth;
    this.password1 = password1;
    this.password2 = password2;
  }
}
