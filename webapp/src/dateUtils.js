export function getTimezoneOffsetMS() {
  const now = new Date();
  return now.getTimezoneOffset() * 60 * 1000;
}
