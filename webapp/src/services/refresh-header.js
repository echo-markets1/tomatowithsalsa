export default function refreshHeader() {
  let JWT = JSON.parse(localStorage.getItem("JWT"));

  if (JWT && JWT.refresh) {
    return { Authorization: "Bearer " + JWT.refresh };
  } else {
    return {};
  }
}
