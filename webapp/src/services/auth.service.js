import axios from "axios";

// TODO Configure for prod
const API_URL = "http://127.0.0.1:8000/";

class AuthService {
  login(user) {
    return axios
      .post(API_URL + "auth/login/", {
        email: user.email,
        password: user.password,
      })
      .then((response) => {
        if (response.data.access) {
          localStorage.setItem("JWT", JSON.stringify(response.data));
        }
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem("JWT");
  }

  refreshToken() {
    let JWT = JSON.parse(localStorage.getItem("JWT"));
    return axios
      .post(API_URL + "auth/refresh/", {
        refresh: JWT.refresh,
      })
      .then((response) => {
        if (response.data.access) {
          localStorage.setItem("JWT", JSON.stringify(response.data));
        }
        return response.data;
      });
    // const user_refresh_token = JSON.parse(localStorage.getItem('JWT')).refresh;
    //localStorage.setItem('JWT', JSON.stringify(response.data));
  }

  verifyEmail(uid, token) {
    return axios.get(API_URL + `users/verify-email/${uid}/${token}/`);
  }

  signup(user) {
    return axios.post(API_URL + "users/signup/", {
      email: user.email,
      first_name: user.first_name,
      last_name: user.last_name,
      date_of_birth: user.date_of_birth,
      password1: user.password1,
      password2: user.password2,
    });
  }
}

export default new AuthService();
