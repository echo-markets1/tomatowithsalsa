import axios from "axios";
//TODO config fo prod
const BASE_API_URL = "http://localhost:8000/";
const axiosWrapper = axios.create({ baseURL: BASE_API_URL });

axiosWrapper.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    // Return any error which is not due to authentication back to the calling service
    if (error.response.status !== 401) {
      return new Promise((resolve, reject) => {
        reject(error);
      });
    }

    // Logout user if token refresh didn't work or user is disabled
    if (
      error.config.url == BASE_API_URL + "/api/account/auth/refresh/" &&
      error.response.data.detail == "Given token not valid for any token type"
    ) {
      this.$store.dispatch("auth/logout");
      this.$router.push("/login");

      return new Promise((resolve, reject) => {
        reject(error);
      });
    }

    // Try request again with new token
    return (
      this.$store
        .dispatch("auth/refreshToken")
        .then((token) => {
          // New request with new token
          const config = error.config;
          config.headers["Authorization"] = `Bearer ${token}`;
          return new Promise((resolve, reject) => {
            axios
              .request(config)
              .then((response) => {
                resolve(response);
              })
              .catch((error) => {
                reject(error);
              });
          });
        })
        // Both refresh and auth tokens are expired, login again necessary
        .catch(() => {
          this.$store.dispatch("auth/logout");
          this.$router.push("/login");
        })
    );
  }
);

export default axiosWrapper;
