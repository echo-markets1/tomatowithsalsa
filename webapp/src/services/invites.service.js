import authHeader from "./auth-header";
import axiosWrapper from "./axios-wrapper";

const TYPES = {
  PLATFORM: "PLATFORM",
  GROUP: "GROUP",
};
const BASE_URL = "api/invites";
class InvitesService {
  async getInvitesFromUser() {
    console.log(`Getting user's ${TYPES.PLATFORM} invites.`);
    return axiosWrapper.get(`${BASE_URL}/${TYPES.PLATFORM}/`, {
      headers: authHeader(),
    });
  }
  async getInvitesToGroup(groupId) {
    console.log(`Getting invites for group with id: ${groupId}.`);
    return axiosWrapper.get(`${BASE_URL}/${TYPES.GROUP}/?group_id=${groupId}`, {
      headers: authHeader(),
    });
  }
  async createPlatformInvite(recipientEmail) {
    return axiosWrapper.post(
      `${BASE_URL}/${TYPES.PLATFORM}/`,
      {
        recipient_email: recipientEmail,
      },
      {
        headers: authHeader(),
      }
    );
  }
  async createGroupInvite(recipientEmail, groupId) {
    return axiosWrapper.post(
      `${BASE_URL}/${TYPES.GROUP}/`,
      {
        recipient_email: recipientEmail,
        group: groupId,
      },
      {
        headers: authHeader(),
      }
    );
  }
  async deleteInvite(type, invite_id) {
    return axiosWrapper.get(`${BASE_URL}/${type}/${invite_id}/delete/`, {
      headers: authHeader(),
    });
  }
  async deletePlatformInvite(invite_id) {
    return this.deleteInvite(TYPES.PLATFORM, invite_id);
  }
  async deleteGroupInvite(invite_id) {
    return this.deleteInvite(TYPES.GROUP, invite_id);
  }
  async acceptInvite(inviteType, inviteIdB64, token) {
    return axiosWrapper.get(
      `${BASE_URL}/accept/${inviteType}/${inviteIdB64}/${token}/`,
      {
        headers: authHeader(),
      }
    );
  }
  async getPlatformReferralLink() {
    return axiosWrapper.get(`${BASE_URL}/referrallink/`, {
      headers: authHeader(),
    });
  }
  async getGroupReferralLink(groupId) {
    return axiosWrapper.get(
      `${BASE_URL}/referrallink/${TYPES.GROUP}/${groupId}/`,
      {
        headers: authHeader(),
      }
    );
  }
  /**
   * Accept/redeem a referral link
   * @param {str} inviteType - The type of the referral (group, platform, etc.)
   * @param {str} referringUserId - The uid of the user who provided the link, base64 encoded
   * @param {str} acceptingUserId - The uid of the user redeeming the link, base64 encoded
   * @param {str} token - The token uniquely validating the link
   * @param {str} relatedEntityId - The uid of the entity the referral relates to, base 64 encoded
   *   For example, a referral link for a group would require the uid of the group to redeem
   */
  async redeemReferralLink(
    inviteType,
    referringUserId,
    acceptingUserId,
    token,
    relatedEntityId
  ) {
    return axiosWrapper.post(
      `${BASE_URL}/referrallink/${inviteType}/`,
      {
        referring_user_idb64: referringUserId,
        accepting_user_idb64: acceptingUserId,
        token: token,
        token_model_pkb64: relatedEntityId,
      },
      {
        headers: authHeader(),
      }
    );
  }
}

export default new InvitesService();
