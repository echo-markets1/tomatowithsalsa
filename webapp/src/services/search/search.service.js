import authHeader from "@/services/auth-header";
import axiosWrapper from "@/services/axios-wrapper";
import axios from "axios";

class SearchService {
  async searchGroups(query, page = null, page_size = null) {
    let url = `api/group/search/?q=${query}`;
    if (page != null) {
      url = url + `&page=${page}`;
    }
    if (page_size != null) {
      url = url + `&page_size=${page_size}`;
    }
    return axiosWrapper.get(url, { headers: authHeader() });
  }
  async searchStocks(symbol) {
    const url = `https://api.twelvedata.com/symbol_search?symbol=${symbol}`;
    return axios.get(url, {});
  }
}

export default new SearchService();
