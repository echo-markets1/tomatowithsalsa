import axios from "axios";

const _API_URL = "https://api.twelvedata.com/";
const _URL_SUFFIX = `&country=United States&apikey=${process.env.VUE_APP_TWELVE_DATA_API_KEY}`;

class QuoteService {
  async getQuotes(symbols_list) {
    let symbols = "";
    if (symbols_list.length === 0) {
      return null;
    }
    symbols_list.forEach((symbol) => {
      symbols = symbol + "," + symbols;
    });
    let response = await axios.get(
      _API_URL + `quote?symbol=${symbols}` + _URL_SUFFIX,
      {}
    );
    return response;
  }
  async getQuote(symbol) {
    let response = await axios.get(
      _API_URL + `quote?symbol=${symbol}` + _URL_SUFFIX,
      {}
    );
    return response;
  }
  async getCurrentPrices(symbols_list) {
    let symbols = "";
    if (symbols_list.length === 0) {
      return null;
    }
    symbols_list.forEach((symbol) => {
      symbols = symbol + "," + symbols;
    });
    let response = await axios.get(
      _API_URL + `price?symbol=${symbols}` + _URL_SUFFIX,
      {}
    );
    return response;
  }
}

export default new QuoteService();
