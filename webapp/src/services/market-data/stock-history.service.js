import axios from "axios";
import { GRANULARITIES } from "@/constants/twelvedata-constants.js";
import { getMostRecentTradingDay } from "@/store/market-schedule-checker";

const _BASE_API_URL = `https://api.twelvedata.com/time_series?apikey=${process.env.VUE_APP_TWELVE_DATA_API_KEY}&country=United States&timezone=UTC`;

/**
 * Private function for determining the start_date parameter for the call to twelvedata
 * @param {String} granularity
 * @returns
 */
function _getStartDate(granularity) {
  const now = new Date();
  switch (granularity) {
    case GRANULARITIES.FIVE_MIN: {
      return getMostRecentTradingDay();
    }
    case GRANULARITIES.ONE_HOUR: {
      const oneWeekMS = 604800000;
      const oneWeekAgo = new Date(now.getTime() - oneWeekMS);
      return oneWeekAgo;
    }
    case GRANULARITIES.ONE_DAY: {
      const threeMonthsMS = 7884000000;
      const threeMonthsAgo = new Date(now.getTime() - threeMonthsMS);
      return threeMonthsAgo;
    }
    case GRANULARITIES.ONE_WEEK: {
      const oneYearMS = 31540000000;
      const oneYearAgo = new Date(now.getTime() - oneYearMS);
      return oneYearAgo;
    }
    default:
      throw `twelvedata calls for the interval '${granularity}' are not supported. PortfolioTimeseries data for intervals greater than an 1h will rely on data from the PortfolioHistoryService.`;
  }
}

class StockHistoryService {
  /**
   * Retrieves timeseries data from twelvedata for a list of symbols. Uses the
   * granularity to compute the interval and start_date parameters
   * @param {Array} symbolList - A list of stocks to retrieve timeseries data for
   * @param {String} granularity - The interval between datapoints in the timeseries
   * @returns {Promise} - The response containing timeseries data from twelvedata
   */
  async getTimeseriesData(symbolList, granularity) {
    if (symbolList.length <= 0) {
      throw "List of symbols must be non-empty";
    }
    let url = _BASE_API_URL + `&interval=${granularity}`;
    const symbols = symbolList.join(",");
    url = url + `&symbol=${symbols}`;

    const startDate = _getStartDate(granularity);
    
    const dateString =
      startDate.getFullYear() +
      "-" +
      // JS Date months are 0 indexed, IE Jan = 0, Dec = 11
      (startDate.getMonth() + 1) +
      "-" +
      startDate.getDate();
    url = url + `&start_date=${dateString}`;
    return await axios.get(url);
  }
}

export default new StockHistoryService();
