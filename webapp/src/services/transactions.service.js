import authHeader from "./auth-header";
import axiosWrapper from "./axios-wrapper";

class TransactionsService {
  constructor() {
    this._BASE_URL = "api/portfolios/transactions/";
  }
  async get({ groupId = null, portfolioId = null }) {
    let url = this._BASE_URL;
    if (groupId) {
      url = url + `?group_id=${groupId}&`;
    }
    if (portfolioId) {
      url = url + `?portfolio_id=${portfolioId}`;
    }
    return axiosWrapper.get(url, {
      headers: authHeader(),
    });
  }
  async like(transactionId) {
    return axiosWrapper.get(`${this._BASE_URL}like/${transactionId}/`, {
      headers: authHeader(),
    });
  }
}
export default new TransactionsService();
