export default function authHeader() {
  let JWT = JSON.parse(localStorage.getItem("JWT"));

  if (JWT && JWT.access) {
    return { Authorization: "Bearer " + JWT.access };
  } else {
    return {};
  }
}
