import authHeader from "./auth-header";
import axiosWrapper from "./axios-wrapper";

const BASE_URL = "api/history/composite/timeseries/";
class CompositeHistoryService {
  /**
   * Calls the HOTH service to retrieve the timeseries information at the specific
   * granularity for the given portfolio.
   * @param {String} granularity - The distance in time between datapoints
   * @returns {Promise} - The backend response containing timeseriesData for the
   *  given portfolio and granularity
   */
  getCompositeTimeseriesData(userId = null, granularity = null) {
    let url = BASE_URL;
    if (userId) {
      url = url + `${userId}/`;
    }
    if (granularity) {
      url = url + `?granularity=${granularity}`;
    }
    return axiosWrapper.get(url, { headers: authHeader() });
  }
}

export default new CompositeHistoryService();
