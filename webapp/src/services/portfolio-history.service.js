import authHeader from "./auth-header";
import axiosWrapper from "./axios-wrapper";

class PortfolioHistoryService {
  /**
   * Calls the HOTH service to retrieve the timeseries information at the specific
   * granularity for the given portfolio.
   * @param {String} portfolioId - The unique identifier of the portfolio
   * @param {String} granularity - The distance in time between datapoints
   * @returns {Promise} - The backend response containing timeseriesData for the
   *  given portfolio and granularity
   */
  getPortfolioTimeseriesData(portfolioId, granularity = null) {
    let url = `api/history/portfolio/timeseries/${portfolioId}/`;
    if (granularity) {
      url = url + `?granularity=${granularity}`;
    }
    return axiosWrapper.get(url, { headers: authHeader() });
  }

  getPortfolioReturnData(portfolioId) {
    const url = `api/history/portfolio/return/${portfolioId}/`;
    return axiosWrapper.get(url, { headers: authHeader() });
  }
}

export default new PortfolioHistoryService();
