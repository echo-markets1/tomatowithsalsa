import authHeader from "./auth-header";
import axiosWrapper from "./axios-wrapper";
const BASE_URL = "api/group/";
class GroupsDataService {
  getUserGroups(userId = null) {
    // 50 is current max limit, 10 is default
    let url = `${BASE_URL}?limit=50`;
    if (userId != null) {
      url = url + `&?user_id=${btoa(userId)}`;
    }
    return axiosWrapper.get(url, { headers: authHeader() });
  }
  getMemberships(userId) {
    return axiosWrapper.get(`${BASE_URL}memberships/${btoa(userId)}/`, {
      headers: authHeader(),
    });
  }
  getTopGroups() {
    return axiosWrapper.get(`${BASE_URL}?filter=top`, {
      headers: authHeader(),
    });
  }
  getTrendingGroups() {
    return axiosWrapper.get(`${BASE_URL}?filter=trending`, {
      headers: authHeader(),
    });
  }
  getGroupById(groupId) {
    return axiosWrapper.get(`${BASE_URL}${groupId}/`, {
      headers: authHeader(),
    });
  }
  createGroup(name, description, privacy, portfolioId) {
    return axiosWrapper.post(
      BASE_URL,
      {
        name: name,
        description: description,
        privacy: privacy,
        portfolio_id: portfolioId,
      },
      {
        headers: authHeader(),
      }
    );
  }
  editGroup(groupId, name, description) {
    return axiosWrapper.patch(
      `${BASE_URL}${groupId}/`,
      {
        name: name,
        description: description,
      },
      {
        headers: authHeader(),
      }
    );
  }
  // Also use to accept, request, and invite members
  addMember(groupId, memberId = null, portfolioId = null) {
    return axiosWrapper.post(
      `${BASE_URL}${groupId}/membership/`,
      {
        member_id: memberId,
        portfolio_id: portfolioId,
      },
      {
        headers: authHeader(),
      }
    );
  }
  editMember(groupId, memberId, type) {
    return axiosWrapper.patch(
      `${BASE_URL}${groupId}/membership/${memberId}/`,
      {
        type: type,
      },
      {
        headers: authHeader(),
      }
    );
  }
  deleteMember(groupId, memberId) {
    return axiosWrapper.delete(
      `${BASE_URL}${groupId}/membership/${memberId}/`,
      {
        headers: authHeader(),
      }
    );
  }
}

export default new GroupsDataService();
