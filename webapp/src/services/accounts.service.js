// import axios from 'axios';
import authHeader from "./auth-header";
import axiosWrapper from "./axios-wrapper";

class AccountsService {
  constructor() {
    this._GENERIC_USERS_BASE_URL = "users/";
    this._CURRENT_USER_BASE_URL = this._GENERIC_USERS_BASE_URL + "current/";
  }
  getUsers({ pageSize = 30, pageNumber = 1, groupId = null }) {
    let url = this._GENERIC_USERS_BASE_URL;
    if (groupId != null) {
      url = `${url}?group=${groupId}&`;
    }
    // group must always come first if present, otherwise it is ignored
    url = url + `?page_size=${pageSize}&?page=${pageNumber}`;
    return axiosWrapper.get(url, {
      headers: authHeader(),
    });
  }
  getCurrentAccount() {
    return axiosWrapper.get(this._CURRENT_USER_BASE_URL, {
      headers: authHeader(),
    });
  }
  getUser({ userId = null, handle = null }) {
    if (userId == null && handle == null) {
      throw "Must provide a non null userId or handle to complete this request.";
    } else if (userId != null && handle != null) {
      console.log(
        "Both userId and handle were provided. Making request with userId."
      );
    }
    if (userId != null) {
      return axiosWrapper.get(this._GENERIC_USERS_BASE_URL + userId + "/", {
        headers: authHeader(),
      });
    } else if (handle != null) {
      return axiosWrapper.get(this._GENERIC_USERS_BASE_URL + handle + "/", {
        headers: authHeader(),
      });
    }
  }
  updateHandle(handle) {
    const params = { handle: handle };
    return axiosWrapper.patch(this._CURRENT_USER_BASE_URL, params, {
      headers: authHeader(),
    });
  }
  updateName(firstName = null, lastName = null) {
    const params = {};
    if (firstName != null) {
      params.first_name = firstName;
    }
    if (lastName != null) {
      params.last_name = lastName;
    }
    return axiosWrapper.patch(this._CURRENT_USER_BASE_URL, params, {
      headers: authHeader(),
    });
  }
  updatePassword(currentPassword, newPassword1, newPassword2) {
    const params = {
      current_password: currentPassword,
      new_password_1: newPassword1,
      new_password_2: newPassword2,
    };
    return axiosWrapper.patch(this._CURRENT_USER_BASE_URL, params, {
      headers: authHeader(),
    });
  }
  updateEmail(newEmail1, newEmail2, password) {
    const params = {
      new_email_1: newEmail1,
      new_email_2: newEmail2,
      current_password: password,
    };
    return axiosWrapper.patch(this._CURRENT_USER_BASE_URL, params, {
      headers: authHeader(),
    });
  }
  updateProfile(profile) {
    const params = { profile: profile };
    return axiosWrapper.put(this._CURRENT_USER_BASE_URL, params, {
      headers: authHeader(),
    });
  }
}

export default new AccountsService();
