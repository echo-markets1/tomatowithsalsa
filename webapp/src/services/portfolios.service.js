import authHeader from "./auth-header";
import axiosWrapper from "./axios-wrapper";
const BASE_URL = "api/portfolios";
class PortfoliosService {
  async getUserPortfolios() {
    return await axiosWrapper.get(BASE_URL, { headers: authHeader() });
  }
  async get(portfolioId) {
    console.log(`Getting Portfolio with id: ${portfolioId}`);
    return await axiosWrapper.get(`${BASE_URL}/${portfolioId}/`, {
      headers: authHeader(),
    });
  }
  async getGroupPortfolios(groupId) {
    return await axiosWrapper.get(`${BASE_URL}?group-id=${groupId}`, {
      headers: authHeader(),
    });
  }
  async getPortfolioByUserId(userId) {
    return await axiosWrapper.get(`${BASE_URL}?user-id=${userId}`, {
      headers: authHeader(),
    });
  }
  async link(name, description) {
    console.log(
      `Creating Portfolio with name: ${name} and description: ${description}`
    );
    // return await axiosWrapper.post(
    //   "api/portfolios/",
    //   {
    //     name: name,
    //     description: description,
    //   },
    //   {
    //     headers: authHeader(),
    //   }
    // );
  }
  async unlink(portfolioId) {
    console.log(`Removing Portfolio with id: ${portfolioId}`);
    // return await axiosWrapper.post(`api/portfolios/${portfolioId}/`, {
    //   headers: authHeader(),
    // });
  }
  async patch(portfolioId, payload) {
    console.log(
      `Updating Portfolio with id: ${portfolioId} to: ${payload.name} - ${payload.description}`
    );
    return await axiosWrapper.patch(`${BASE_URL}${portfolioId}/`, payload, {
      headers: authHeader(),
    });
  }
}

export default new PortfoliosService();
