import authHeader from "./auth-header";
import axiosWrapper from "./axios-wrapper";

const TYPES = {
  PORTFOLIO: "PORTFOLIO",
};
class RiskMetricsService {
  // TODO: [https://app.clickup.com/t/1cd95r0] Update to support periods
  async get(type, id) {
    console.log(`Getting risk metrics for ${type} with id: ${id}`);
    return axiosWrapper.get(`api/history/riskmetrics/${type}/${id}/`, {
      headers: authHeader(),
    });
  }
  async getPortfolioRiskMetrics(id) {
    return this.get(TYPES.PORTFOLIO, id);
  }
}

export default new RiskMetricsService();
