import authHeader from "./auth-header";
import axiosWrapper from "./axios-wrapper";

const TYPES = {
  WORST: "WORST",
  BEST: "BEST",
};
/**
 * Wrapper class to make BE calls for superlative performance metric data,
 * which corresponds to the best/worst day/week/month performance metrics
 */
class SuperlativePerformanceMetricsService {
  async get(type, portfolioId) {
    console.log(`Getting ${type} DWM metrics for portfolio: ${portfolioId}`);
    return axiosWrapper.get(
      `api/history/portfolio/superlativeperformancemetrics/${type}/${portfolioId}/`,
      {
        headers: authHeader(),
      }
    );
  }
  async getBestDWMMetrics(portfolioId) {
    return this.get(TYPES.BEST, portfolioId);
  }
  async getWorstDWMMetrics(portfolioId) {
    return this.get(TYPES.WORST, portfolioId);
  }
}

export default new SuperlativePerformanceMetricsService();
