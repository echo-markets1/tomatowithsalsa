import authHeader from "../auth-header";
import axiosWrapper from "../axios-wrapper";

class PlaidLinkService {
  async createLinkToken() {
    return await axiosWrapper.post(
      "/api/plaid-link-management/create-link-token",
      {},
      {
        headers: authHeader(),
      }
    );
  }

  async setAccessToken(public_token, metadata) {
    return await axiosWrapper.post(
      "/api/plaid-link-management/set-access-token",
      {
        public_token: public_token,
        accounts: metadata.accounts,
        institution: metadata.institution,
        link_session_id: metadata.link_session_id,
      },
      {
        headers: authHeader(),
      }
    );
  }
}

export default new PlaidLinkService();
