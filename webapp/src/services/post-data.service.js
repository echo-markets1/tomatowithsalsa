import authHeader from "./auth-header";
import axiosWrapper from "./axios-wrapper";

class PostDataService {
  // To call these functions with default values, an empty object ({}) must be provided
  // Ex: postDataService.getTopPosts({})
  async getPosts({
    pageSize = 30,
    pageNumber = 1,
    ordering = null,
    transactionId = null,
  }) {
    let url = `api/posts/?`;
    if (ordering != null) {
      url = url + `ordering=${ordering}&`;
    }
    if (transactionId != null) {
      url = url + `transaction_id=${transactionId}&`;
    }
    // ordering must always come first, otherwise it is ignored
    url = url + `page=${pageNumber}&page_size=${pageSize}`;
    return axiosWrapper.get(url, { headers: authHeader() });
  }
  async getNewestPosts({
    pageSize = 30,
    pageNumber = 1,
    transactionId = null,
  }) {
    return this.getPosts({
      pageSize: pageSize,
      pageNumber: pageNumber,
      ordering: "-created_at",
      transactionId: transactionId,
    });
  }
  async getTopPosts({ pageSize = 30, pageNumber = 1 }) {
    return this.getPosts({
      pageSize: pageSize,
      pageNumber: pageNumber,
      ordering: "-total_activity",
    });
  }
  async createPost({ message, transactionId = null }) {
    const url = `api/posts/`;
    return axiosWrapper.post(
      url,
      {
        message: message,
        transaction_id: transactionId,
        superpost_id: null,
      },
      {
        headers: authHeader(),
      }
    );
  }
  async likePost({ postId }) {
    const url = `api/posts/${postId}/like/`;
    return axiosWrapper.get(url, {
      headers: authHeader(),
    });
  }
}

class GroupPostDataService {
  async getPosts({
    groupId,
    pageSize = 30,
    pageNumber = 1,
    ordering = null,
    transactionId = null,
  }) {
    let url = `api/posts/group/${groupId}/?`;
    if (ordering != null) {
      url = url + `ordering=${ordering}&`;
    }
    if (transactionId != null) {
      url = url + `transaction_id=${transactionId}&`;
    }
    // ordering must always come first, otherwise it is ignored
    url = url + `page_size=${pageSize}&page=${pageNumber}`;
    return axiosWrapper.get(url, { headers: authHeader() });
  }

  async getNewestPosts({
    groupId,
    pageSize = 30,
    pageNumber = 0,
    transactionId = null,
  }) {
    return this.getPosts({
      groupId: groupId,
      pageSize: pageSize,
      pageNumber: pageNumber,
      ordering: "-created_at",
      transactionId: transactionId,
    });
  }

  async getTopPosts({ groupId, pageSize = 30, pageNumber = 0 }) {
    return this.getPosts({
      groupId: groupId,
      pageSize: pageSize,
      pageNumber: pageNumber,
      ordering: "-total_activity",
    });
  }

  async createPost({ groupId, message, transactionId = null }) {
    const url = `api/posts/group/${groupId}/`;
    return axiosWrapper.post(
      url,
      {
        message: message,
        superpost_id: null,
        transaction_id: transactionId,
      },
      {
        headers: authHeader(),
      }
    );
  }

  async replyToPost({ groupId, superPostId, message }) {
    const url = `api/posts/group/${groupId}/`;
    return axiosWrapper.post(
      url,
      {
        message: message,
        superpost_id: superPostId,
      },
      {
        headers: authHeader(),
      }
    );
  }

  async likePost({ postId }) {
    const url = `api/posts/${postId}/like/`;
    return axiosWrapper.get(url, {
      headers: authHeader(),
    });
  }

  async pinPost({ postId }) {
    const url = `api/posts/${postId}/pin/`;
    return axiosWrapper.get(url, {
      headers: authHeader(),
    });
  }
}

export const postDataService = new PostDataService();
export const groupPostDataService = new GroupPostDataService();
