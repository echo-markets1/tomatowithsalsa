import Vue from "vue";
import VueRouter from "vue-router";
import Registration from "@/views/Registration.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("../views/Home.vue"),
  },
  {
    path: "/token-info",
    name: "token-info",
    // lazy-loaded
    component: () => import("../views/TokenInfo.vue"),
  },
  {
    path: "/verify-email/:uid/:token",
    name: "verify-email",
    component: () => import("@/views/VerifyEmail.vue"),
  },
  {
    // Path for processing invites
    path: "/accept-invite/:typ/:id/:token/",
    props: (route) => ({
      invite: {
        typ: route.params.typ,
        id: route.params.id,
        token: route.params.token,
      },
    }),
    name: "registration",
    component: Registration,
  },
  {
    // Path for processing referral links
    path: "/referral/:typ/:tokenRelatedEntityId/:token/:invitorId/",
    props: (route) => ({
      referral: {
        typ: route.params.typ,
        tokenRelatedEntityId: route.params.tokenRelatedEntityId,
        token: route.params.token,
        invitorId: route.params.invitorId,
      },
    }),
    name: "registration",
    component: Registration,
  },
  {
    // Default registration/login path
    path: "/registration",
    name: "registration",
    component: Registration,
    alias: ["/login"],
  },
  {
    path: "/profile/:userId",
    name: "profile",
    component: () => import("@/views/Profile.vue"),
    props: true
  },
  {
    path: "/group/:groupId",
    component: () => import("@/views/Group.vue"),
    props: true,
    children: [
      {
        path: "/",
        name: "group-overview",
        component: () => import("@/components/group/tabs/GroupOverview.vue"),
        props: true,
      },
      {
        path: "trade-feed",
        name: "group-trade-feed",
        component: () => import("@/components/group/tabs/GroupTradeLog.vue"),
        props: true,
      },
      // TODO
      // {
      //   path: "chat",
      //   name: "group-chat",
      // },
      {
        path: "members",
        name: "group-members",
        component: () => import("@/components/group/GroupMembers.vue"),
      },
      // TODO
      // {
      //   path: "competition",
      //   name: "group-competition",
      // },
    ],
  },
  {
    path: "/portfolio/:portfolioId",
    component: () => import("@/views/ManagePortfolios.vue"),
    props: true,
    children: [
      {
        path: "analysis",
        name: "portfolio-analysis",
        props: true,
        component: () =>
          import("@/components/buckets/tabs/PortfolioAnalysis.vue"),
      },
      {
        path: "transactions",
        name: "portfolio-transactions",
        props: true,
        component: () =>
          import("@/components/buckets/tabs/PortfolioTransactions.vue"),
      },
      {
        path: "/",
        name: "portfolio-overview",
        props: true,
        component: () =>
          import("@/components/buckets/tabs/PortfolioOverview.vue"),
      },
    ],
  },
  {
    path: "/stock/:tickerSymbol/",
    component: () => import("@/views/StockDetail.vue"),
    props: true,
    children: [
      {
        path: "/",
        name: "stock-overview",
        props: true,
        component: () => import("@/components/stock/tabs/StockOverview.vue"),
      },
    ],
  },
  {
    path: "/stock/:tickerSymbol/:quoteCurrencySymbol/",
    component: () => import("@/views/StockDetail.vue"),
    props: true,
    children: [
      {
        path: "/",
        name: "stock-overview",
        props: true,
        component: () => import("@/components/stock/tabs/StockOverview.vue"),
      },
    ],
  },
  {
    path: "*",
    name: "page-not-found",
    component: () => import("../views/PageNotFound.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const publicPages = ["verify-email", "registration"];
  const authRequired = !publicPages.includes(to.name);
  const loggedIn = localStorage.getItem("JWT");
  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    next("/login");
  } else {
    next();
  }
});

export default router;
