/*
Define the events used to communicate across sibling components (ie: Those used
with an eventBus). globalEvents/EventBusses should be a last resort, since global
events can lead to tight coupling and difficult to follow logic. However, they are
occassionally a neccessary evil. By defining the events in a universal location,
we can at least make the logic easier to search and follow, and limit the chances
of event collision (pushing/consuming events with unintended consequences)
*/

const SHOW_CASH_CHANGE = "show-cash-change";
const SHOW_PERCENT_CHANGE = "show-percent-change";
const SET_FOCUS_ON_SEARCH = "set-focus-on-search";
const SHOW_GROUP_POST_DETAIL_MODAL = "show-group-post-detail-modal";
const SHOW_TRANSACTION_POST_DETAIL_MODAL =
  "show-group-transaction-detail-modal";

export const GLOBAL_EVENTS = {
  SHOW_CASH_CHANGE,
  SHOW_PERCENT_CHANGE,
  SET_FOCUS_ON_SEARCH,
  SHOW_GROUP_POST_DETAIL_MODAL,
  SHOW_TRANSACTION_POST_DETAIL_MODAL,
};
