// Granularities, based on https://twelvedata.com/docs#stocks
const FIVE_MIN = "5min";
const ONE_HOUR = "1h";
const ONE_DAY = "1day";
const ONE_WEEK = "1week";

export const GRANULARITIES = {
  FIVE_MIN,
  ONE_HOUR,
  ONE_DAY,
  ONE_WEEK,
};
