/**
 * Directive to help simplify and abstract the logic of adding a 'is-valid' or
 * 'is-invalid' class when input has been validated
 */
const VALID_CLASS = " is-valid";
const INVALID_CLASS = " is-invalid";

function setValidatedClassName(el, bindingValue) {
  // Add the appropriate class to the component based on the validation status
  if (bindingValue === true) {
    // Modify className vs classList to stay compatible with older browsers
    el.className = el.className.replaceAll(INVALID_CLASS, "");
    if (!el.className.includes(VALID_CLASS)) {
      el.className += VALID_CLASS;
    }
  } else if (bindingValue === false) {
    el.className = el.className.replaceAll(VALID_CLASS, "");
    if (!el.className.includes(INVALID_CLASS)) {
      el.className += INVALID_CLASS;
    }
  } else {
    el.className = el.className.replaceAll(INVALID_CLASS, "");
    el.className = el.className.replaceAll(VALID_CLASS, "");
  }
}

export default {
  bind: function(el, binding) {
    setValidatedClassName(el, binding.value);
  },
  componentUpdated: function(el, binding) {
    setValidatedClassName(el, binding.value);
  },
  unbind: function(el) {
    el.className = el.className.replaceAll(INVALID_CLASS, "");
    el.className = el.className.replaceAll(VALID_CLASS, "");
  },
};
