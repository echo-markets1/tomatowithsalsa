# Overview

To learn more about the tomatowithsalsa project, the technologies involved,
and the services involved, see the
[project overview](https://share-docs.clickup.com/2268607/p/h/257dz-1887/b08feaf6b082ad3) on
ClickUp.

# Development

To learn more about using and developing for the tomatowithsalsa project,
see the [development docs](https://share-docs.clickup.com/2268607/p/h/257dz-1907/2837e3972043b0a)
on ClickUp

## Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
